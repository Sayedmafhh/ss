package ae.mazadat.onlineauctionapp.model.rest;

import org.parceler.Parcel;

/**
 * Created by agile on 4/17/16.
 */
@Parcel
public class MZOption {
    String id;
    String name;
    String details;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }
}
