package ae.mazadat.onlineauctionapp.adapter.menu;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.adapter.BaseAdapter;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.ui.ItemsListFragment;
import ae.mazadat.onlineauctionapp.viewholder.ItemViewHolder;
import ae.mazadat.onlineauctionapp.viewholder.MyBidItemViewHolder;

/**
 * {@link RecyclerView.Adapter} that can display a {@link MZItem} and makes a call to the
 * specified {@link ItemsListFragment.OnMZItemInteractionListener}.
 */
public class MyBidsAdapter extends BaseAdapter {

    public MyBidsAdapter(Context context, ItemsListFragment.OnMZItemInteractionListener listener) {
        super(context, listener);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_bid, parent, false);

        final MyBidItemViewHolder vh = new MyBidItemViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder vh, int position) {

        final MyBidItemViewHolder holder = (MyBidItemViewHolder) vh;
        holder.mItem = mValues.get(position);

        holder.tvItemName.setText(holder.mItem.getItemName());

        holder.mBids.setText(holder.mItem.getBids() + "");
        holder.mPrice.setText(holder.mItem.getAmount() + "");
        holder.price2.setText(holder.mItem.getPrice() + "");

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onMZItemInteraction(holder.mItem, AGConf.KEY_MY_BIDS);
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(String.valueOf(mValues.get(position).getId()));
    }

}
