package ae.mazadat.onlineauctionapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.MZItemDetails;
import ae.mazadat.onlineauctionapp.ui.ItemsListFragment;
import ae.mazadat.onlineauctionapp.viewholder.DuItemViewHolder;
import ae.mazadat.onlineauctionapp.viewholder.ItemViewHolder;

/**
 * {@link RecyclerView.Adapter} that can display a {@link MZItem} and makes a call to the
 * specified {@link ItemsListFragment.OnMZItemInteractionListener}.
 */
public class NumbersAdapter extends BaseAdapter {
    private boolean isActivatedBid = true;
    private Context context;

    public NumbersAdapter(Context context, ItemsListFragment.OnMZItemInteractionListener listener, boolean isActivatedBid) {
        super(context, listener);
        this.context = context;
        this.isActivatedBid = isActivatedBid;
    }

//    @Override
//    public int getItemViewType(int position) {
//        return mValues.get(position).getCity();
//    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_du_small, parent, false);

        final DuItemViewHolder vh = new DuItemViewHolder(view);
        return vh;
    }

    public void onViewRecycled(ItemViewHolder vh) {
        super.onViewRecycled(vh);

        final DuItemViewHolder holder = (DuItemViewHolder) vh;
        holder.vTag.setBackgroundResource(R.color.colorItemNormal);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder vh, int position) {

        final DuItemViewHolder holder = (DuItemViewHolder) vh;

        // Reset style
        holder.vTag.setBackgroundColor(mContext.getResources().getColor(R.color.colorItemNormal));
        holder.vTag.setAlpha(1);

        holder.mItem = mValues.get(position);
        System.out.println("child "+mValues.toString());
        synchronized (lstHolders) {
            lstHolders.add(holder);
        }

        MZItemDetails details = holder.mItem.getDetails();
        if (details != null && details.getNumber() != null) {
            String number = holder.mItem.getDetails().getNumber();
            holder.tvPrefix.setText(number.substring(0, 3));
            holder.tvNumber.setText(number.substring(3));

            if (holder.mItem.getBuyNow().equalsIgnoreCase("1")){
                holder.lblBids.setText(R.string.buy_now);
                holder.counterLayout.setVisibility(View.GONE);
                holder.divider.setVisibility(View.GONE);
                holder.soldLayout.setVisibility(View.GONE);
            }else if (holder.mItem.getBuyNow().equalsIgnoreCase("0")){
                holder.lblBids.setText(R.string.lbl_bids);
                holder.divider.setVisibility(View.VISIBLE);
                holder.counterLayout.setVisibility(View.VISIBLE);
                holder.soldLayout.setVisibility(View.GONE);
            }else {
                holder.soldLayout.setVisibility(View.VISIBLE);
                holder.soldLayout.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                    }
                });
            }
        }

        holder.mPrice.setText(String.format("%,d", holder.mItem.getPrice()));
        if (holder.mItem.getBuyNow().equalsIgnoreCase("1")){
            holder.mBids.setVisibility(View.GONE);

        }else{
            holder.mBids.setText(mContext.getString(R.string.bids, holder.mItem.getBids()));
        }
        holder.mBids.setText(mContext.getString(R.string.bids, holder.mItem.getBids()));
        holder.mView.setTag(R.string.position, position);

        holder.tvPackage.setVisibility(View.VISIBLE);
        holder.tvPackage.setText(details.getDuPackage());

        if(holder.mItem.getBuyNow().equalsIgnoreCase("1")){
            holder.vTag.setBackgroundResource(R.color.colorItemRed);
            holder.hummerImg.setImageDrawable(context.getResources().getDrawable(R.mipmap.cart));
        }else if (holder.mItem.isMyBid()) {
            holder.vTag.setBackgroundResource(R.color.colorItemGreen);
            holder.hummerImg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_bid));
        } else {
            if (holder.mItem.isBided()) {
//                blink(holder, 0);
                blink(holder);
            }
        }
        holder.mItem.getCategory();

        // Change the string for date time as per the format
        if (holder.mItem.isOnlineAuction() == false) {
            holder.txtViewBidDateTime.setText(android.text.format.DateFormat.format("MM-dd-yyyy HH:MM", holder.mItem.getEndTime()) + "");
            holder.txtViewBidDateTime.setVisibility(View.VISIBLE);
            holder.llBidDateTime.setVisibility(View.VISIBLE);
            holder.priceCounter.setVisibility(View.GONE);
            holder.mBids.setVisibility(View.GONE);
            holder.lblBids.setVisibility(View.GONE);
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onMZItemInteraction(holder.mItem, AGConf.KEY_DU_AUCTION);
//                    mListener.onMZItemInteractionNew(holder.mItem, AGConf.KEY_DU_AUCTION);
                }
            }
        });

        holder.updateTimeRemaining();

    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(String.valueOf(mValues.get(position).getId()));
    }

}
