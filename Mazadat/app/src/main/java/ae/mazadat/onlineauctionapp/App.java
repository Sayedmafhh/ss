package ae.mazadat.onlineauctionapp;

import android.app.Application;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.firebase.client.Firebase;

import ae.mazadat.onlineauctionapp.util.FontsOverride;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import ae.mazadat.onlineauctionapp.util.rest.AGRestClient;
import io.fabric.sdk.android.Fabric;
import retrofit2.Retrofit;

/**
 * Created by agile on 3/23/16.
 */
public class App extends Application {

    private static App mInstance;
    private static SessionManager sessionManager;
    private static Retrofit restAdapter;
    private static Context appContext;
    private static SoundPool soundPool;
    private static AudioManager am;

    public static synchronized App getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Answers());
        Fabric.with(this, new Crashlytics());

        Firebase.setAndroidContext(this);

        mInstance = this;

        appContext = getApplicationContext();
        sessionManager = new SessionManager(appContext);
        if (sessionManager.getLang().equals("ar")) {
            FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/DroidKufi_Regular.ttf");
        } else {
            FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/MyriadPro_Regular.ttf");
        }

        restAdapter = AGRestClient.getAdapter();
        soundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 100);
        am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);


    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    public static AudioManager getAudioManager() {
        return am;
    }

    public static Retrofit getRestAdapter() {
        return restAdapter;
    }

    public static SessionManager getSessionManager() {
        return sessionManager;
    }

    public static SoundPool getSoundPool() {
        return soundPool;
    }

    public static Context getAppContext() {
        return appContext;
    }

}
