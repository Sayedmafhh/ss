package ae.mazadat.onlineauctionapp.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by IT_3 on 9/24/2017.
 */

@Parcel
    public class TotalValue {

        @SerializedName("commision")
        String commision;
        @SerializedName("administration_fees")
        String administrationFees;
        @SerializedName("price")
        String price;
        @SerializedName("cart_subtotal")
        int cartSubtotal;
        @SerializedName("quantity")
        int quantity;
        @SerializedName("shipping_charge")
        String shippingCharge;

        public String getCommision() {
            return commision;
        }

        public void setCommision(String commision) {
            this.commision = commision;
        }

        public String getAdministrationFees() {
            return administrationFees;
        }

        public void setAdministrationFees(String administrationFees) {
            this.administrationFees = administrationFees;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public int getCartSubtotal() {
            return cartSubtotal;
        }

        public void setCartSubtotal(int cartSubtotal) {
            this.cartSubtotal = cartSubtotal;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public String getShippingCharge() {
            return shippingCharge;
        }

        public void setShippingCharge(String shippingCharge) {
            this.shippingCharge = shippingCharge;
        }

    }


