package ae.mazadat.onlineauctionapp.model.rest;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by agile on 5/22/16.
 */
@Parcel
public class Deposit {
    @SerializedName("user_deposit")
    int userDeposit;

    @SerializedName("vip")
    int isVip;

    @SerializedName("is_auto_bid")
    int isAutoBid;

    @SerializedName("max_auto_bid")
    int maxBid;

    public int getUserDeposit() {
        return userDeposit;
    }

    public int isVip() {
        return isVip;
    }

    public int isAutoBid() {
        return isAutoBid;
    }

    public int getMaxBid() {
        return maxBid;
    }
}
