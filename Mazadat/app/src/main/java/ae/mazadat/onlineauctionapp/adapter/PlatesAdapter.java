package ae.mazadat.onlineauctionapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.MZItemDetails;
import ae.mazadat.onlineauctionapp.ui.ItemsListFragment;
import ae.mazadat.onlineauctionapp.util.MZUtil;
import ae.mazadat.onlineauctionapp.viewholder.ItemViewHolder;
import ae.mazadat.onlineauctionapp.viewholder.PlateItemViewHolder;

/**
 * {@link RecyclerView.Adapter} that can display a {@link MZItem} and makes a call to the
 * specified {@link ItemsListFragment.OnMZItemInteractionListener}.
 */
public class PlatesAdapter extends BaseAdapter {
    private Context context;
    public PlatesAdapter(Context context, ItemsListFragment.OnMZItemInteractionListener listener) {
        super(context, listener);
        this.context = context;
    }


    @Override
    public int getItemViewType(int position) {
        return mValues.get(position).getCity();
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_plate_small, parent, false);

        final PlateItemViewHolder vh = new PlateItemViewHolder(view);
        return vh;
    }

    // TODO: Test
    @Override
    public void onViewRecycled(ItemViewHolder vh) {
        super.onViewRecycled(vh);

        final PlateItemViewHolder holder = (PlateItemViewHolder) vh;

//        holder.ivPlate.setImageDrawable(null);
//        holder.vTag.setBackgroundResource(R.color.colorItemNormal);
//        if(holder.mItem.getBuyNow().equalsIgnoreCase("1")){
//            holder.vTag.setBackgroundResource(R.color.colorItemRed);
//            holder.hummerImg.setImageDrawable(context.getResources().getDrawable(R.mipmap.cart));
//        }else if (holder.mItem.isMyBid()) {
//            holder.vTag.setBackgroundResource(R.color.colorItemNormal);
//            holder.hummerImg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_bid));
//        }
    }

    @Override
    public void onBindViewHolder(ItemViewHolder vh, int position) {

        final PlateItemViewHolder holder = (PlateItemViewHolder) vh;

        // Reset style
        holder.vTag.setBackgroundColor(mContext.getResources().getColor(R.color.colorItemNormal));
        holder.vTag.setAlpha(1);

        holder.mItem = mValues.get(position);

        synchronized (lstHolders) {
            lstHolders.add(holder);
        }

        holder.ivPlate.setImageDrawable(null);

        MZItemDetails details = holder.mItem.getDetails();
        if (details != null && details.getPlateLetter() != null && details.getNumber() != null) {

            String type = holder.mItem.getDetails().getPlateType();

            if (holder.mItem.getCity() == 7) { // Dubai
                if (type.contentEquals("3")) {
                    holder.tvPltR.setTextColor(mContext.getResources().getColor(R.color.white));
                    holder.tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                } else if (type.contentEquals("2")) {
                    holder.tvPltR.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryText));
                    holder.tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                } else {
                    holder.tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    holder.tvPltR.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryText));
                }
            } else {
                if (type.contentEquals("2")) { // Motocycle
                    holder.tvMotocycle.setVisibility(View.VISIBLE);
                } else {
                    holder.tvMotocycle.setVisibility(View.GONE);
                }

                holder.tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                holder.tvPltR.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryText));
            }
            if (holder.mItem.getBuyNow().equalsIgnoreCase("1")){
                holder.lblBids.setText(R.string.buy_now);
                holder.counterLayout.setVisibility(View.GONE);
                holder.divider.setVisibility(View.GONE);

            }else{
                holder.lblBids.setText(R.string.lbl_bids);
                holder.divider.setVisibility(View.VISIBLE);
                holder.counterLayout.setVisibility(View.VISIBLE);
            }
            holder.ivPlate.setImageDrawable(MZUtil.getPlateDrawable(holder.mItem.getCity(), type));

//            if (holder.mItem.getCity() == 10) { // Abu Dhabi
//                holder.tvPltL.setTextColor(mContext.getResources().getColor(R.color.white));
//            } else {
//                holder.tvPltL.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryText));
//            }

            holder.tvPltL.setText(holder.mItem.getDetails().getPlateLetter());
            holder.tvPltR.setText(holder.mItem.getDetails().getNumber());

//            holder.ivPlate.setImageDrawable(MZUtil.getPlateDrawable(holder.mItem.getCity()));
//
//            holder.tvPltL.setText(holder.mItem.getDetails().getPlateLetter());
//            holder.tvPltR.setText(holder.mItem.getDetails().getNumber());
        }
        if(holder.mItem.getBuyNow().equalsIgnoreCase("1")){
            holder.vTag.setBackgroundResource(R.color.colorItemRed);
            holder.hummerImg.setImageDrawable(context.getResources().getDrawable(R.mipmap.cart));
        }else if (holder.mItem.isMyBid()) {
            holder.vTag.setBackgroundResource(R.color.colorItemNormal);
            holder.hummerImg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_bid));
        }
        holder.mPrice.setText(String.format("%,d", holder.mItem.getPrice()));
//        holder.mBids.setText(mContext.getString(R.string.bids, holder.mItem.getBids()));
        if (holder.mItem.getBuyNow().equalsIgnoreCase("1")){
            holder.mBids.setVisibility(View.GONE);

        }else{
            holder.mBids.setText(mContext.getString(R.string.bids, holder.mItem.getBids()));
        }

//        MZUtil.setTimer(mContext, holder);

//        holder.mMinBid.setText(holder.mItem.getMinBid() + "");
//        holder.mDate.setText(AGUtil.formatMazad(holder.mItem.getEndTime()));

//        holder.mCountDown.setText(holder.mItem.getBids());


//        ValueAnimator animator = new ValueAnimator();
//        animator.setObjectValues(0, 600);
//        animator.setDuration(5000);
//
//        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            public void onAnimationUpdate(ValueAnimator animation) {
//                holder.mCountDown.setText("" + (int) animation.getAnimatedValue());
//            }
//        });
//        animator.start();


//        _setCountDown(holder, holder.mItem.getCurrentTime(), holder.mItem.getEndTime(),
//                holder.tvC3, holder.tvC2, holder.tvC1, holder.tvC3T);


//        holder.mView.setTag(R.string.position, position);
        if (holder.mItem.isMyBid()) {
            holder.vTag.setBackgroundResource(R.color.colorItemGreen);
        } else {
            if (holder.mItem.isBided()) {
//                blink(holder, 0);
                blink(holder);
            }
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onMZItemInteraction(holder.mItem, AGConf.KEY_PLATES_AUCTION);
                }
            }
        });

        holder.updateTimeRemaining();
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(String.valueOf(mValues.get(position).getId()));
    }
}
