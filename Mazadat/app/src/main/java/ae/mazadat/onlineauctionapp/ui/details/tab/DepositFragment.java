package ae.mazadat.onlineauctionapp.ui.details.tab;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.parceler.Parcels;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.rest.Deposit;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by agile on 4/22/16.
 */
public class DepositFragment extends Fragment {

    // Details
    @Bind(R.id.tvDeposit)
    TextView tvDeposit;

    @Bind(R.id.tvUserDeposit)
    TextView tvUserDeposit;

    @Bind(R.id.tvToPay)
    TextView tvToPay;

    @Bind(R.id.tvRemain)
    TextView tvRemain;

    static String deposit;
    static String userDeposit;
    static String toPay;
    static String remain;

    public DepositFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     * @param item
     * @param deposit
     */
    public static DepositFragment newInstance(MZItem item, Deposit deposit) {
        DepositFragment fragment = new DepositFragment();
        Bundle args = new Bundle();
        args.putParcelable(AGConf.KEY_ITEM_DATA, Parcels.wrap(item));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        tvDeposit.setText(deposit);
        tvUserDeposit.setText(userDeposit);
        tvToPay.setText(toPay);
        tvRemain.setText(remain);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_deposit, container, false);

        ButterKnife.bind(this, rootView);
        return rootView;
    }
}
