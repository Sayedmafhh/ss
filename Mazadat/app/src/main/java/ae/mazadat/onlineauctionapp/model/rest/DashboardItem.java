package ae.mazadat.onlineauctionapp.model.rest;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by agile on 4/14/16.
 */
@Parcel
public class DashboardItem {
    int id;
    String name;
    @SerializedName("url")
    String url;
    @SerializedName("icon")
    String icon;
    @SerializedName("alias")
    String alias;

    @SerializedName("total_items")
    int total;

    @SerializedName("items")
    List<DashboardItem> subItems;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getTotal() {
        return total;
    }

    public String getAlias() {
        return alias;
    }

    public List<DashboardItem> getSubItems() {
        return subItems;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setSubItems(List<DashboardItem> subItems) {
        this.subItems = subItems;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
