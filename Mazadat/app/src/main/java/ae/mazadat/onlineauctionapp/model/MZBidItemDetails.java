package ae.mazadat.onlineauctionapp.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

/**
 * Created by agile on 3/31/16.
 */

@Parcel
public class MZBidItemDetails {

    @SerializedName("id")
    public String id;

//    @SerializedName("category")
//    public String category;

    @SerializedName("end_time")
    Date endTime;

//    @SerializedName("start_bid")
//    Date startBid;

    MZItemDetails details;

    @SerializedName("min_bid")
    public int minBid; // Min Bid

    @SerializedName("max_bid")
    public int price; // Price

    @SerializedName("bids")
    public int bids; // Bids count

    public String getId() {
        return id;
    }

//    public String getCategory() {
//        return category;
//    }

    public Date getEndTime() {
        return endTime;
    }

//    public Date getStartBid() {
//        return startBid;
//    }

    public MZItemDetails getDetails() {
        return details;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public void setDetails(MZItemDetails details) {
        this.details = details;
    }

    public int getMinBid() {
        return minBid;
    }

    public void setMinBid(int minBid) {
        this.minBid = minBid;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getBids() {
        return bids;
    }

    public void setBids(int bids) {
        this.bids = bids;
    }
}
