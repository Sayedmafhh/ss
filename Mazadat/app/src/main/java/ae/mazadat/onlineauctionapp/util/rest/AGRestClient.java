package ae.mazadat.onlineauctionapp.util.rest;

import android.text.Html;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by agile on 3/23/16.
 */
public class AGRestClient {
    private static Retrofit _adapter;

    public AGRestClient() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(new ItemTypeAdapterFactory())
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        // Enable logging
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                SessionManager session = App.getSessionManager();

                Request.Builder request = original.newBuilder()
                        .header("Accept", "application/json")
                        .method(original.method(), original.body());

                if (session.isLoggedIn()) {
                    String token = session.getToken();
                    request.header("X-MZ-token", token);
                }

                Response response = chain.proceed(request.build());

                // Customize or return the response
                return response;
            }
        });

        OkHttpClient client = httpClient.build();


        Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(AGConf.REST_API)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                ;

        _adapter = restAdapter;
    }

    public Retrofit _getAdapter() {
        return _adapter;
    }


    /**
     * Singleton for getting single API service adapter
     * @return
     */
    public static Retrofit getAdapter() {
        if (_adapter == null) {
            _adapter = new AGRestClient()._getAdapter();
        }

        return _adapter;
    }



    /**
     * Created by agile on 1/21/15.
     */
    public static class ItemTypeAdapterFactory implements TypeAdapterFactory {

        public <T> TypeAdapter<T> create(Gson gson, final TypeToken<T> type) {

            final TypeAdapter<T> delegate = gson.getDelegateAdapter(this, type);
            final TypeAdapter<JsonElement> elementAdapter = gson.getAdapter(JsonElement.class);

            return new TypeAdapter<T>() {

                public void write(JsonWriter out, T value) throws IOException {
                    delegate.write(out, value);
                }

                public T read(JsonReader in) throws IOException {

                    JsonElement jsonElement = elementAdapter.read(in);
                    if (jsonElement.isJsonObject()) {
                        JsonObject jsonObject = jsonElement.getAsJsonObject();
                        if (jsonObject.has("data"))
                        {
                            jsonElement = jsonObject.get("data");
                        }


                        // Handle errors here...
                        if (jsonObject.has("status") && jsonObject.get("status").getAsInt() != 200)
                        {
                            String msg = "";
                            if (jsonObject.has("msg")) {
                                if (!jsonObject.get("msg").getAsString().isEmpty()) {
                                    msg = Html.fromHtml(jsonObject.get("msg").getAsString()).toString();
                                } else {
                                    msg = "An error has been occured! Code: " + jsonObject.get("status").getAsString();
                                }
                            }
                        }
                    }

                    return delegate.fromJsonTree(jsonElement);
                }
            }.nullSafe();
        }
    }
}
