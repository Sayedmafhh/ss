package ae.mazadat.onlineauctionapp.adapter.menu;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.adapter.BaseAdapter;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.MZItemDetails;
import ae.mazadat.onlineauctionapp.ui.ItemsListFragment;
import ae.mazadat.onlineauctionapp.util.MZUtil;
import ae.mazadat.onlineauctionapp.viewholder.ItemViewHolder;
import ae.mazadat.onlineauctionapp.viewholder.SellItemViewHolder;

/**
 * {@link RecyclerView.Adapter} that can display a {@link MZItem} and makes a call to the
 * specified {@link ItemsListFragment.OnMZItemInteractionListener}.
 */
public class MyPurchasesAdapter extends BaseAdapter {

    public MyPurchasesAdapter(Context context, ItemsListFragment.OnMZItemInteractionListener listener) {
        super(context, listener);
        auctionType = AGConf.KEY_AUCTION_GRID;
    }

    @Override
    public int getItemViewType(int position) {
        return mValues.get(position).getCity();
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_plate_sell, parent, false);

        final SellItemViewHolder vh = new SellItemViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder vh, int position) {

        final SellItemViewHolder holder = (SellItemViewHolder) vh;
        holder.mItem = mValues.get(position);

        synchronized (lstHolders) {
            lstHolders.add(holder);
        }

        holder.ivPlate.setImageDrawable(null);
//        holder.ivSoldOut.setVisibility(View.GONE);

        MZItemDetails details = holder.mItem.getDetails();
        if (details != null && details.getPlateLetter() != null && details.getNumber() != null) {

            holder.ivPlate.setImageDrawable(MZUtil.getPlateDrawable(holder.mItem.getCity(), holder.mItem.getDetails().getPlateType()));

//            if (holder.mItem.getCity() == 10) { // Abu Dhabi
//                holder.tvPltL.setTextColor(mContext.getResources().getColor(R.color.white));
//            } else {
//                holder.tvPltL.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryText));
//            }

            holder.tvPltL.setText(holder.mItem.getDetails().getPlateLetter());
            holder.tvPltR.setText(holder.mItem.getDetails().getNumber());
        }

//        if (holder.mItem.getSoldBy() != null && !holder.mItem.getSoldBy().contentEquals("0")) {
//            holder.ivSoldOut.setVisibility(View.VISIBLE);
//        }

        holder.mPrice.setText(String.format("%,d", holder.mItem.getSellPrice()));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onMZItemInteraction(holder.mItem, AGConf.KEY_MY_PURCHASES);
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(String.valueOf(mValues.get(position).getId()));
    }

}
