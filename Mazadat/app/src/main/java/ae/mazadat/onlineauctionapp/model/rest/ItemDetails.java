package ae.mazadat.onlineauctionapp.model.rest;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by agile on 4/3/16.
 */
@Parcel
public class ItemDetails {

    String number;
    String letter;

    @SerializedName("plate_type")
    int plateType;

    // Du
    @SerializedName("package_name")
    String duPackage;


    public String getNumber() {
        return number;
    }

    public String getLetter() {
        return letter;
    }

    public int getPlateType() {
        return plateType;
    }

    public String getDuPackage() {
        return duPackage;
    }

}

