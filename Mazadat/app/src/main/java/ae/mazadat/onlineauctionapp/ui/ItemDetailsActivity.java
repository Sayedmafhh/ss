package ae.mazadat.onlineauctionapp.ui;

import android.annotation.TargetApi;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import butterknife.Bind;

public class ItemDetailsActivity extends BaseActivity {

    @Bind(R.id.content_frame)
    View container;

//    public boolean isMyBid = false;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setHasDrawer(false);

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        }

        setTitle("");

        setContentView(R.layout.activity_item_details);

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);

            final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
            actionBar.setHomeAsUpIndicator(upArrow);
        }

        String auctionType = getIntent().getStringExtra(AGConf.KEY_AUCTION_TYPE);
        Parcelable data = getIntent().getParcelableExtra(AGConf.KEY_ITEM_DATA);

        if (auctionType == null || data == null) {
            finish();
        } else {

            Fragment fragment;

            // Direct Sell
            if (auctionType.contentEquals(AGConf.KEY_DIRECT_SELL)) {
                fragment = new DirectSellDetailsFragment();
            } else {
                fragment = new ItemDetailsFragment();
            }

            Bundle args = new Bundle();

            args.putParcelable(AGConf.KEY_ITEM_DATA, data);
            args.putString(AGConf.KEY_AUCTION_TYPE,  auctionType);

            // Disable title
            if (getSupportActionBar() != null) {
//                getSupportActionBar().setDisplayShowTitleEnabled(false);
            }

            if (auctionType.contentEquals(AGConf.KEY_DU_AUCTION)) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window.setStatusBarColor(getResources().getColor(R.color.colorDUDark));
                }
                toolbar.setBackgroundColor(getResources().getColor(R.color.colorDU));

                ivDu.setVisibility(View.VISIBLE);

            } else {
                // Error !
            }

            fragment.setArguments(args);

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
//            navigateUpTo(new Intent(this, MainActivity.class));
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
