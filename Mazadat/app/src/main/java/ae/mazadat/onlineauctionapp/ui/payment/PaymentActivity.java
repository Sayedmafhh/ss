package ae.mazadat.onlineauctionapp.ui.payment;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.ui.BaseActivity;
import butterknife.Bind;

public class PaymentActivity extends BaseActivity {

    @Bind(R.id.tvTitle)
    TextView tvTitle;

    @Bind(R.id.etAmount)
    EditText etAmount;

    @Bind(R.id.btnPay)
    Button btnPay;

    int category = 0;

    // Direct sell
    int itemId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasDrawer(false);

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        }

        setContentView(R.layout.activity_payment);

        setTitle(getString(R.string.title_payment));

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
            actionBar.setHomeAsUpIndicator(upArrow);
        }

        String type = getIntent().getStringExtra(AGConf.KEY_AUCTION_TYPE);
        if (type != null) {
            tvTitle.setText(getString(R.string.amount_deposit));
            if (type.contentEquals(AGConf.KEY_PLATES_AUCTION)) {
                category = AGConf.CAT_PLATES;
            }

            else if(type.contentEquals(AGConf.KEY_DU_AUCTION)) {
                category = AGConf.CAT_DU;
            }

            else if(type.contentEquals(AGConf.KEY_CARS_AUCTION)) {
                category = AGConf.CAT_CARS;
            }

            else if(type.contentEquals(AGConf.KEY_BAGS_AUCTION)) {
                category = AGConf.CAT_BAGS;
            }

            else if(type.contentEquals(AGConf.KEY_COINS_AUCTION)) {
                category = AGConf.CAT_COINS;
            }

            else if(type.contentEquals(AGConf.KEY_PROP_AUCTION)) {
                category = AGConf.CAT_PROP;
            }

            else if(type.contentEquals(AGConf.KEY_HORSES_AUCTION)) {
                category = AGConf.CAT_HORSE;
            }

            else if(type.contentEquals(AGConf.KEY_JEWELLERY_AUCTION)) {
                category = AGConf.CAT_WATCH;
            }

            else if(type.contentEquals(AGConf.KEY_MISC_AUCTION)) {
                category = AGConf.CAT_MISC;
            }

            // Direct sell
            else if(type.contentEquals(AGConf.KEY_DIRECT_SELL)) {
                category = AGConf.CAT_DIRECT_SELL;
                itemId   = getIntent().getIntExtra(AGConf.KEY_DIRECT_SELL_ITEM_ID, 0);

                // TODO: Handle this case....
            }
        }

//        int amount = getIntent().getIntExtra(AGConf.KEY_DEPOSIT_AMOUNT, 0);
//        etAmount.setText(amount + "");

        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String amount = etAmount.getText().toString();
                if (amount.isEmpty()) {
                    Toast.makeText(PaymentActivity.this, getString(R.string.err_no_payment), Toast.LENGTH_SHORT).show();
                } else if(Integer.parseInt(amount) < 1000) {
                    Toast.makeText(PaymentActivity.this, getString(R.string.err_min_payment), Toast.LENGTH_SHORT).show();
                } else {
                    if (Integer.parseInt(amount) > 0) {
                        PaymentWebActivity.startPayment(AGConf.PAYMENT_URL + "?category="+ category +"&amount=" + etAmount.getText(), PaymentActivity.this);
                    } else {
                        Toast.makeText(PaymentActivity.this, getString(R.string.err_no_payment), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }

        return true;
    }

}

