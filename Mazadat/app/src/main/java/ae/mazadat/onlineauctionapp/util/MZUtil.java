package ae.mazadat.onlineauctionapp.util;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import java.io.File;

import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;

/**
 * Created by agile on 4/7/16.
 */
public class MZUtil {



    /**
     * Get Plate background drawable
     * @param city
     * @return
     */
    public static Drawable getPlateDrawable(int city) {

        switch (city) {
            case 7:
                return App.getAppContext().getResources().getDrawable(R.drawable.plate_7);

            case 10:
                return App.getAppContext().getResources().getDrawable(R.drawable.plate_10);

            case 11:
                return App.getAppContext().getResources().getDrawable(R.drawable.plate_11);

            case 12:
                return App.getAppContext().getResources().getDrawable(R.drawable.plate_12);

            case 13:
                return App.getAppContext().getResources().getDrawable(R.drawable.plate_13);

            case 14:
                return App.getAppContext().getResources().getDrawable(R.drawable.plate_14);

            case 15:
                return App.getAppContext().getResources().getDrawable(R.drawable.plate_15);

            default:
                return null;
        }
    }

    public static Drawable getPlateDrawable(int city, String type) {

        switch (city) {
            case 7:
                if (type.contentEquals("2")) {
                    return App.getAppContext().getResources().getDrawable(R.drawable.plate_7_m);
                } else if (type.contentEquals("3")) {
                    return App.getAppContext().getResources().getDrawable(R.drawable.plate_7_c);
                } else {
                    return App.getAppContext().getResources().getDrawable(R.drawable.plate_7);
                }

            case 10:
                return App.getAppContext().getResources().getDrawable(R.drawable.plate_10);

            case 11:
                return App.getAppContext().getResources().getDrawable(R.drawable.plate_11);

            case 12:
                return App.getAppContext().getResources().getDrawable(R.drawable.plate_12);

            case 13:
                return App.getAppContext().getResources().getDrawable(R.drawable.plate_13);

            case 14:
                return App.getAppContext().getResources().getDrawable(R.drawable.plate_14);

            case 15:
                return App.getAppContext().getResources().getDrawable(R.drawable.plate_15);

            default:
                return null;
        }
    }

    public static void downloadFile(Context context, String url) {
        if (url == null) {
            return;
        }

        Toast.makeText(context, context.getString(R.string.download_start), Toast.LENGTH_SHORT).show();
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/PxlApp");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        // TODO: Check permission
        DownloadManager mgr = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);

        Uri downloadUri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle(context.getString(R.string.saving_image))
                .setDescription(context.getString(R.string.download_drawed_image))
                .setDestinationInExternalPublicDir("/PxlApp", getFileNameFromUrl(url));

        mgr.enqueue(request);
    }

    public static String getFileNameFromUrl(String url) {
        return url.substring(url.lastIndexOf('/') + 1).split("\\?")[0].split("#")[0];
    }

    public static void doWhenSuccessBid(Activity activity, String msg) {
        Answers.getInstance().logCustom(new CustomEvent("Post Bid").putCustomAttribute("success", "Yes"));
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
        activity.finish();
    }
}
