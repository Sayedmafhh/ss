package ae.mazadat.onlineauctionapp.ui;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.parceler.Parcels;

import java.util.List;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.MZLoc;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sudhirkumar on 4/10/2017.
 */

public class LocationActivityNew extends BaseActivity {
    @Bind(R.id.tvLocation)
    TextView tvLocation;
    @Bind(R.id.mapView)
    MapView mapView;
    @Bind(R.id.backBtn)
    ImageView backBtn;
    private GoogleMap mMap;
    MZItem mItem;
    String currentAuction = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        }

        setContentView(R.layout.activity_location_new);
        ButterKnife.bind(this);
        String auctionType = getIntent().getStringExtra(AGConf.KEY_AUCTION_TYPE);
        Parcelable data = getIntent().getParcelableExtra(AGConf.KEY_ITEM_DATA);
        mItem = Parcels.unwrap(data);

        if (mItem != null) {
            System.out.println("not null****** " + mItem.getPrice());
        } else {
            System.out.println("null******");
        }
        currentAuction = auctionType;
        if (getIntent().getData() != null) {

        }

        final String locationCoords = mItem.getDetails().getLocationCoords();

//        tvLocation.setText(locationCoords);

        mapView.onCreate(savedInstanceState);

        mapView.onResume();// needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;

                double latitude  = 25.274187;
                double longitude = 55.340312;

                double brLatitude;
                double brLongitude;
                String title;
                List<MZLoc> BranchLoc = mItem.getDetails().getBranchLocations();

                if (BranchLoc != null && !BranchLoc.isEmpty()) {
                    // String[] coords = locationCoords.split(",");
                    for (int j = 0; j <= 2; j++) {
                        // latitude and longitude
                        title = BranchLoc.get(j).getTitle();
                        brLatitude  = Double.parseDouble(BranchLoc.get(j).getLatitude());
                        brLongitude = Double.parseDouble(BranchLoc.get(j).getLongitude());

                        // create marker
                        MarkerOptions marker = new MarkerOptions().position(
                                new LatLng(brLatitude, brLongitude)).title(title);

                        // Changing marker icon
                        marker.icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

                        // adding marker
                        mMap.addMarker(marker);
                    }
                }


                if (locationCoords != null && !locationCoords.isEmpty()) {
                    String[] coords = locationCoords.split(",");
                    // latitude and longitude
                    latitude = Double.parseDouble(coords[0]);
                    longitude = Double.parseDouble(coords[1]);

                    // create marker
                    MarkerOptions marker = new MarkerOptions().position(
                            new LatLng(latitude, longitude)).title("Location");

                    // Changing marker icon
                    marker.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

                    // adding marker
                    mMap.addMarker(marker);
                }

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(latitude, longitude)).zoom(9).build();
                mMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
            }
        });

    }
}
