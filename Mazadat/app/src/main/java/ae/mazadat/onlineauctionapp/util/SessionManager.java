package ae.mazadat.onlineauctionapp.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.List;

import ae.mazadat.onlineauctionapp.model.Dashboard;
import ae.mazadat.onlineauctionapp.model.rest.DashboardItem;
import ae.mazadat.onlineauctionapp.model.rest.LoginResponse;
import ae.mazadat.onlineauctionapp.model.rest.MZUser;
import ae.mazadat.onlineauctionapp.ui.LoginActivity;
import ae.mazadat.onlineauctionapp.ui.MainActivity;

/**
 * Created by agile on 3/23/16.
 */
public class SessionManager {

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "MazadatOnline";

    private static final String IS_LOGIN = "IsLoggedIn";

    // Token
    public static final String KEY_TOKEN = "token";
    public static final String KEY_NAME = "username";
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_USER_IMG = "user_img";
    public static final String KEY_LANG = "lang";

    // PN
    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERTY_APP_VERSION = "appVersion";

    public static final String KEY_PLAT_DEPOSIT  = "plate_deposit";
    public static final String KEY_DU_DEPOSIT    = "du_deposit";
    public static final String KEY_CAR_DEPOSIT   = "car_deposit";
    public static final String KEY_BAG_DEPOSIT   = "bag_deposit";
    public static final String KEY_WATCH_DEPOSIT = "watch_deposit";
    public static final String KEY_MIX_DEPOSIT   = "mix_deposit";
    public static final String KEY_PROP_DEPOSIT  = "prop_deposit";
    public static final String KEY_HORSE_DEPOSIT = "horse_deposit";
    public static final String KEY_COIN_DEPOSIT  = "coin_deposit";

    private static final String DASHBOARD = "dashoard_items";

    private boolean loggedIn;

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public String getToken () {
        return pref.getString(KEY_TOKEN, null);
    }

    public void handleLogin (){

        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, LoginActivity.class); // LoginActivity
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            _context.startActivity(i);
        } else {
            Intent i = new Intent(_context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            _context.startActivity(i);
        }

    }

    public void createLoginSession(LoginResponse response) {
        MZUser user  = response.getUser();
        String name  = user.getName();
        String token = user.getToken();
        String email = user.getEmail();
        String lang  = user.getLang();
        String phone = user.getPhone();
        int      id  = user.getId();

        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing session in pref
        editor.putString(KEY_TOKEN, token);
        editor.putString(KEY_NAME,  name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_PHONE, phone);
        editor.putString(KEY_LANG,  lang.contentEquals("ae") ? "ar" : "en");
        editor.putInt(KEY_USER_ID,  id);

        // commit changes
        editor.commit();
    }

    public void saveDashboard(List<DashboardItem> items) {
        Dashboard db = new Dashboard(items);
        String gsonDB = new Gson().toJson(db);
        editor.putString(DASHBOARD, gsonDB);
        editor.commit();
    }

    public List<DashboardItem> getDashboard() {
        String dbString = pref.getString(DASHBOARD, null);

        if (dbString != null ) {
            Dashboard db = new Gson().fromJson(dbString, Dashboard.class);
            return db.getItems();
        }
        return null;
    }


    public String getLang() {
        return pref.getString(KEY_LANG, "ar");
    }


    public String getMZLang() {
        if (pref.getString(KEY_LANG, "ar").contentEquals("ar")) {
            return "ae";
        } else {
            return "en";
        }
    }

    public void logout() {
        editor.clear();
        editor.commit();
    }


    public void setPlatesDeposit(int deposit) {
        editor.putInt(KEY_PLAT_DEPOSIT, deposit);
        editor.commit();
    }

    public void setDuDeposit(int deposit) {
        editor.putInt(KEY_DU_DEPOSIT, deposit);
        editor.commit();
    }

    public void setCarsDeposit(int deposit) {
        editor.putInt(KEY_CAR_DEPOSIT, deposit);
        editor.commit();
    }

    public void setBagsDeposit(int deposit) {
        editor.putInt(KEY_BAG_DEPOSIT, deposit);
        editor.commit();
    }

    public void setWatchesDeposit(int deposit) {
        editor.putInt(KEY_WATCH_DEPOSIT, deposit);
        editor.commit();
    }

    public void setHorsesDeposit(int deposit) {
        editor.putInt(KEY_HORSE_DEPOSIT, deposit);
        editor.commit();
    }

    public void setCoinsDeposit(int deposit) {
        editor.putInt(KEY_COIN_DEPOSIT, deposit);
        editor.commit();
    }

    public void setMixDeposit(int deposit) {
        editor.putInt(KEY_MIX_DEPOSIT, deposit);
        editor.commit();
    }

    public void setPropDeposit(int deposit) {
        editor.putInt(KEY_PROP_DEPOSIT, deposit);
        editor.commit();
    }

    public boolean isTermsAgreed(String currentAuction) {
        return pref.getBoolean(currentAuction, false);
    }

    public void setTermsAgreed(String currentAuction) {
        editor.putBoolean(currentAuction, true);
        editor.commit();
    }


    public void setLang(String lang) {
        editor.putString(KEY_LANG, lang);
        editor.commit();
    }


    public int getPlatesDeposit() {
        return pref.getInt(KEY_PLAT_DEPOSIT, 0);
    }


    public int getUid() {
        return pref.getInt(KEY_USER_ID, 0);
    }

    public String getUserName() {
        return pref.getString(KEY_NAME, "");
    }

    public String getUserEmail() {
        return pref.getString(KEY_EMAIL, "");
    }

    public String getUserPhone() {
        return pref.getString(KEY_PHONE, "");
    }

    public String getUserImage() {
        return pref.getString(KEY_USER_IMG, "");
    }

    public String getRegistrationId() {
        return pref.getString(PROPERTY_REG_ID, "");
    }

    public int getAppVersion() {
        return pref.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
    }

    public void setRegistrationId(String device_id) {
        editor.putString(PROPERTY_REG_ID, device_id);
        editor.commit();
    }
}
