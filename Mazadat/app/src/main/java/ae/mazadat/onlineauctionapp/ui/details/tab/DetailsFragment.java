package ae.mazadat.onlineauctionapp.ui.details.tab;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.parceler.Parcels;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.rest.Deposit;
import ae.mazadat.onlineauctionapp.model.rest.MZValue;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by agile on 4/22/16.
 */
public class DetailsFragment extends Fragment {

    public DetailsFragment() {
    }

//    @Bind(R.id.tvNotes)
//    TextView tvNotes;

    @Bind(R.id.container)
    ViewGroup container;

    MZItem mItem;
    String mType;

    LayoutInflater li;


    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static DetailsFragment newInstance(MZItem item, String type) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(AGConf.KEY_ITEM_DATA, Parcels.wrap(item));
        args.putString(AGConf.KEY_AUCTION_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    public static DetailsFragment newInstance(MZItem item, String currentAuction, Deposit deposit) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(AGConf.KEY_ITEM_DATA, Parcels.wrap(item));
        args.putParcelable(AGConf.KEY_ITEM_DEPOSIT, Parcels.wrap(deposit));
        args.putString(AGConf.KEY_AUCTION_TYPE, currentAuction);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().getParcelable(AGConf.KEY_ITEM_DATA) != null) {
                Parcelable data = getArguments().getParcelable(AGConf.KEY_ITEM_DATA);
                mItem = Parcels.unwrap(data);
            }

            if (getArguments().getString(AGConf.KEY_AUCTION_TYPE) != null) {
                mType = getArguments().getString(AGConf.KEY_AUCTION_TYPE);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_details, container, false);

        ButterKnife.bind(this, rootView);

        li = LayoutInflater.from(getActivity());

//        if (mType != null && mType.contentEquals(AGConf.KEY_CARS_AUCTION) && mItem.getDetails().getNotes() != null) {
        // TODO: Format it
//            tvNotes.setText(Html.fromHtml(mItem.getDetails().getNotes()));
//        }


        // NO need for && mType.contentEquals(AGConf.KEY_CARS_AUCTION)
        if (mType != null && mItem.getDetails().getValues() != null
                && mItem.getDetails().getValues().size() > 0) {

            for (MZValue item : mItem.getDetails().getValues()) {
                _drawItem(item.getLabel(), item.getValue(), false);
            }
        }

        return rootView;
    }

    private void _drawItem(String lbl, String val, boolean hideHr) {
        View v = li.inflate(R.layout.lbl_val, null);
        TextView tvLbl = (TextView) v.findViewById(R.id.tvLbl);
        TextView tvVal = (TextView) v.findViewById(R.id.tvVal);

        tvLbl.setText(lbl);
        tvVal.setText(val);

        if (hideHr) {
            View hr = v.findViewById(R.id.hr);
            hr.setVisibility(View.GONE);
        }

        container.addView(v);
    }

}
