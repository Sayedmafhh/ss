package ae.mazadat.onlineauctionapp.util.pn;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.util.SessionManager;

/**
 * Created by agile on 7/18/16.
 */
public class AGInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        SessionManager session = App.getSessionManager();
        session.setRegistrationId(refreshedToken);
    }
}