package ae.mazadat.onlineauctionapp.model;

import java.util.List;

import ae.mazadat.onlineauctionapp.model.rest.DashboardItem;

/**
 * Created by agile on 4/15/16.
 */
public class Dashboard {

    List<DashboardItem> items;

    public Dashboard(List<DashboardItem> items) {
        this.items = items;
    }

    public List<DashboardItem> getItems() {
        return items;
    }

    public void setItems(List<DashboardItem> items) {
        this.items = items;
    }
}
