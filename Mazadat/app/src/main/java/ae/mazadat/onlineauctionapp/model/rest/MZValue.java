package ae.mazadat.onlineauctionapp.model.rest;

import org.parceler.Parcel;

/**
 * Created by agile on 4/17/16.
 */
@Parcel
public class MZValue {
    String label;
    String value;

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;

    }

    @Override
    public String toString() {
        return "MZValue{" +
                "label='" + label + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
