package ae.mazadat.onlineauctionapp.model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;
import java.util.List;

import ae.mazadat.onlineauctionapp.model.rest.MZImg;

/**
 * Created by agile on 3/31/16.
 */

@Parcel
public class MZItemUsa {

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("title")
    public String title;

    @SerializedName("odometer")
    public String odometer;

    @SerializedName("category")
    public int category;

    @SerializedName("offer")
    public int offer;

    @SerializedName("featured_img")
    public String featured_img;

    @SerializedName("end_time")
    Date endTime;

    @SerializedName("city")
    public int city;

    @Nullable
    @SerializedName("type")
    String type;

//    @SerializedName("start_bid")
//    Date startBid;

    MZItemDetails details;

// USA cars  TODO : Obaid shah
    @Nullable
    @SerializedName("img")
    List<MZImg> img;

    @Nullable
    @SerializedName("shareUrl")
    String shareUrl;

    @Nullable
    @SerializedName("vin")
    String vin;

    @Nullable
    @SerializedName("endDate")
    String endDate;

    @Nullable
    @SerializedName("fuel")
    String fuel;

    @Nullable
    @SerializedName("engine")
    String engine;

    @Nullable
    @SerializedName("drive")
    String drive;

    @Nullable
    @SerializedName("bodyStyle")
    String bodyStyle;

    @Nullable
    @SerializedName("exteriorColor")
    String exteriorColor;

    @Nullable
    @SerializedName("miles")
    String miles;

    @Nullable
    @SerializedName("fees")
    String fees;

    @Nullable
    @SerializedName("desc")
    String desc;

    @Nullable
    @SerializedName("termsCondition")
    String termsCondition;

    @Nullable
    @SerializedName("hTOffer")
    String hTOffer;

    @Nullable
    @SerializedName("hTBuy")
    String hTBuy;

    @Nullable
    @SerializedName("shippingDetails")
    String shippingDetails;


    @Nullable
    @SerializedName("buy_now")
    String buyNow;



    public int getId() {
        return id;
    }

    public Date getEndTime() {
        return endTime;
    }

    @Nullable
    public String getBuyNow() {
        return buyNow;
    }

    public void setBuyNow(@Nullable String buyNow) {
        this.buyNow = buyNow;
    }

    public MZItemDetails getDetails() {
        return details;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }


    @Nullable
    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getCity() {
        return city;
    }


    @Nullable
    public String getType() {
        return type;
    }

    public void setType(@Nullable String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getTitle() {
        return title;
    }

    public String getOdometer() {
        return odometer;
    }

    public int getOffer() {
        return offer;
    }

        @Nullable
        public String getFeatured_img() {
        return featured_img;
    }

        @Nullable
        public List<MZImg> getImg() {
        return img;
    }

        public void setImg(List<MZImg> img) {
        this.img = img;
    }
        @Nullable
        public String getShareUrl() {
        return shareUrl;
    }

        @Nullable
        public String getVin() {
        return vin;
    }

        @Nullable
        public String getEndDate() {
        return endDate;
    }

        @Nullable
        public String getFuel() {
        return fuel;
    }

        @Nullable
        public String getEngine() {
        return engine;
    }

        @Nullable
        public String getDrive() {
        return drive;
    }

        @Nullable
        public String getBodyStyle() {
        return bodyStyle;
    }

        @Nullable
        public String getExteriorColor() {
        return exteriorColor;
    }

        @Nullable
        public String getMiles() {
        return miles;
    }

        @Nullable
        public String getFees() {
        return fees;
    }
        @Nullable
        public String getDesc() {
        return desc;
    }
        @Nullable
        public String getTermsCondition(){
        return termsCondition;
    }
        @Nullable
        public String gethTOffer() {
        return hTOffer;}
        @Nullable
        public String gethTBuy() {
        return hTBuy;}
    @Nullable
    public String getShippingDetails() {
        return shippingDetails;
    }






    @Override
    public String toString() {
        return "MZItem{" +
                "id=" + id +
                ", category=" + category +
                ", endTime=" + endTime +
                ", city=" + city +
                ", type='" + type + '\'' +
                ", details=" + details +
                '}';
    }
}
