package ae.mazadat.onlineauctionapp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import ae.mazadat.onlineauctionapp.model.draw.MZDrawNumberItem;
import butterknife.ButterKnife;

/**
 * Created by agile on 4/1/16.
 */
public class DrawItemViewHolder extends RecyclerView.ViewHolder {

    public final View mView;

    public MZDrawNumberItem mItem;

    public DrawItemViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
        mView = view;
    }
}
