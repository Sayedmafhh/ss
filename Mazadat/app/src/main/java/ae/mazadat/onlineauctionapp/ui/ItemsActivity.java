package ae.mazadat.onlineauctionapp.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

import org.parceler.Parcels;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.rest.DashboardItem;
import ae.mazadat.onlineauctionapp.ui.auction.BagsFragment;
import ae.mazadat.onlineauctionapp.ui.auction.CarsFragment;
import ae.mazadat.onlineauctionapp.ui.auction.CoinsFragment;
import ae.mazadat.onlineauctionapp.ui.auction.DirectSellFragment;
import ae.mazadat.onlineauctionapp.ui.auction.HorsesFragment;
import ae.mazadat.onlineauctionapp.ui.auction.MixFragment;
import ae.mazadat.onlineauctionapp.ui.auction.NumbersFragment;
import ae.mazadat.onlineauctionapp.ui.auction.PlatesFragment;
import ae.mazadat.onlineauctionapp.ui.auction.PropFragment;
import ae.mazadat.onlineauctionapp.ui.auction.WatchesFragment;
import ae.mazadat.onlineauctionapp.ui.services.AdsFragment;
import ae.mazadat.onlineauctionapp.ui.services.DrawNumberFragment;
import ae.mazadat.onlineauctionapp.ui.services.DrawPlateFragment;
import ae.mazadat.onlineauctionapp.ui.services.EvaluatePlateFragment;
import ae.mazadat.onlineauctionapp.ui.services.RequestNumberFragment;
import ae.mazadat.onlineauctionapp.ui.services.RequestPlateFragment;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import ae.mazadat.onlineauctionapp.util.view.RevealBackgroundView;
import butterknife.Bind;

public class ItemsActivity extends DrawerActivity
    implements
        RevealBackgroundView.OnStateChangeListener {

    private static final Interpolator DECELERATE_INTERPOLATOR = new DecelerateInterpolator();

    @Bind(R.id.coordinator)
    CoordinatorLayout vCoordinator;

    @Bind(R.id.content_frame)
    View vContainer;

    @Bind(R.id.vRevealBackground)
    RevealBackgroundView vRevealBackground;

    private boolean pendingIntro = true;
    private int currentState;

    int fillColor = 0xFFFFFFFF;  // 0xFFD32F2F

    ObjectAnimator fadeIn;
    final AnimatorSet mAnimationSet = new AnimatorSet();


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasDrawer(false);

        setContentView(R.layout.activity_items);

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        }

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);

            final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
            actionBar.setHomeAsUpIndicator(upArrow);
        }

        fillColor = getIntent().getIntExtra(AGConf.ARG_REVEAL_COLOR, fillColor);

        fadeIn = ObjectAnimator.ofFloat(vCoordinator, "alpha", .0f, 1f);
        fadeIn.setDuration(400);

        toolbar.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                toolbar.getViewTreeObserver().removeOnPreDrawListener(this);
                pendingIntro = true;
                vContainer.setTranslationY(vContainer.getHeight());
                vCoordinator.setAlpha(0);

                return true;
            }
        });

        setupRevealBackground(savedInstanceState, fillColor);

        Parcelable pItem = getIntent().getParcelableExtra(AGConf.KEY_AUCTION_ITEM);
        DashboardItem dbItem = Parcels.unwrap(pItem);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        Fragment fragment = null;
        String title      = "";
        Bundle args = new Bundle();

        if (dbItem.getId() == AGConf.CAT_DU) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(getResources().getColor(R.color.colorDUDark));
            }
            toolbar.setBackgroundColor(getResources().getColor(R.color.colorDU));

            fragment = new NumbersFragment();
            args.putBoolean(AGConf.KEY_IS_EMPTY_AUCTION, dbItem.getTotal() == 0);

            ivDu.setVisibility(View.VISIBLE);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayShowTitleEnabled(false);
            }
        }

        else if (dbItem.getId() == AGConf.CAT_CARS) {
            fragment = new CarsFragment();
            args.putBoolean(AGConf.KEY_IS_EMPTY_AUCTION, dbItem.getTotal() == 0);
            title = getString(R.string.db_cars);
        }

        else if (dbItem.getId() == AGConf.CAT_COINS) {
            fragment = new CoinsFragment();
            args.putBoolean(AGConf.KEY_IS_EMPTY_AUCTION, dbItem.getTotal() == 0);
            title = getString(R.string.db_money);
        }

        else if (dbItem.getId() == AGConf.CAT_HORSE) {
            fragment = new HorsesFragment();
            args.putBoolean(AGConf.KEY_IS_EMPTY_AUCTION, dbItem.getTotal() == 0);
            title = getString(R.string.db_horses);
        }

        else if (dbItem.getId() == AGConf.CAT_PROP) {
            fragment = new PropFragment();
            args.putBoolean(AGConf.KEY_IS_EMPTY_AUCTION, dbItem.getTotal() == 0);
            title = getString(R.string.db_prop);
        }

        else if (dbItem.getId() == AGConf.CAT_MISC) {
            fragment = new MixFragment();
            args.putBoolean(AGConf.KEY_IS_EMPTY_AUCTION, dbItem.getTotal() == 0);
            title = getString(R.string.db_misc);
        }

        else if (dbItem.getId() == AGConf.CAT_WATCH) {
            fragment = new WatchesFragment();
            args.putBoolean(AGConf.KEY_IS_EMPTY_AUCTION, dbItem.getTotal() == 0);
            title = getString(R.string.db_watches);
        }

        else if (dbItem.getId() == AGConf.CAT_BAGS) {
            fragment = new BagsFragment();
            args.putBoolean(AGConf.KEY_IS_EMPTY_AUCTION, dbItem.getTotal() == 0);
            title = getString(R.string.db_bags);
        }

        else if (dbItem.getId() == AGConf.CAT_PLATES) {
            fragment = new PlatesFragment();
            args.putBoolean(AGConf.KEY_IS_EMPTY_AUCTION, dbItem.getTotal() == 0);
            title = getString(R.string.db_plates);
        }

        // Services
        else if (dbItem.getId() == AGConf.CAT_SERVICES) {
            fragment = new ServicesFragment();
            title = getString(R.string.db_services);
        }

        // DirectSell
        else if (dbItem.getId() == AGConf.CAT_DIRECT_SELL) {
            fragment = new DirectSellFragment();
            title = getString(R.string.db_direct);
        }

        // DirectSell
        else if (dbItem.getId() == AGConf.CAT_DRAW_PLATE) {
            fragment = new DrawPlateFragment();
            title = getString(R.string.db_direct);
        }

        // Evalute
        else if (dbItem.getId() == AGConf.CAT_EVALUATE_PLATE) {
            fragment = new EvaluatePlateFragment();
        }

        // Request plate
        else if (dbItem.getId() == AGConf.CAT_REQUEST_PLATE) {
            fragment = new RequestPlateFragment();
            title = getString(R.string.db_request_plate);
        }

        // Request number
        else if (dbItem.getId() == AGConf.CAT_REQUEST_NUMBER) {
            fragment = new RequestNumberFragment();
            title = getString(R.string.db_request_number);
        }

        // Draw number
        else if (dbItem.getId() == AGConf.CAT_DRAW_NUMBER) {
            fragment = new DrawNumberFragment();
            title = getString(R.string.db_draw_number);
        }

        // Ads
        else if (dbItem.getId() == AGConf.CAT_MIX_ADS) {
            fragment = new AdsFragment();
            title = getString(R.string.db_ads);
        }

        else {
            fragment = null;
        }

        title = dbItem.getName();

        args.putInt(AGConf.KEY_AUCTION_TYPE, dbItem.getId());
        fragment.setArguments(args);

        ft.replace(R.id.content_frame, fragment);
        ft.commit();

        // set the toolbar title
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }


    private void setupRevealBackground(Bundle savedInstanceState, int fillColor) {
        vRevealBackground.setFillPaintColor(fillColor);
        vRevealBackground.setOnStateChangeListener(this);
        if (savedInstanceState == null) {
            final int[] startingLocation = getIntent().getIntArrayExtra(AGConf.ARG_REVEAL_START_LOCATION);
            vRevealBackground.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    vRevealBackground.getViewTreeObserver().removeOnPreDrawListener(this);
                    vRevealBackground.startFromLocation(startingLocation);
                    return true;
                }
            });
        } else {
            vRevealBackground.setToFinishedFrame();
        }
    }

    public static void startFromLocation(int[] startingLocation,
                                         Activity startingActivity, DashboardItem item, int color) {
        // Check login for these items
        SessionManager session = App.getSessionManager();
        if ((item.getId() == AGConf.CAT_DRAW_PLATE || item.getId() == AGConf.CAT_DRAW_NUMBER ||
                item.getId() == AGConf.CAT_REQUEST_PLATE || item.getId() == AGConf.CAT_REQUEST_NUMBER ||
                item.getId() == AGConf.CAT_EVALUATE_PLATE) && !session.isLoggedIn()) {

            Intent intent = new Intent(startingActivity, LoginActivity.class);
            startingActivity.startActivity(intent);
            startingActivity.overridePendingTransition(R.anim.slide_up,R.anim.no_change);
            return;
        }
        Intent intent = new Intent(startingActivity, ItemsActivity.class);
        intent.putExtra(AGConf.ARG_REVEAL_START_LOCATION, startingLocation);
        intent.putExtra(AGConf.ARG_REVEAL_COLOR, color);
        intent.putExtra(AGConf.KEY_AUCTION_ITEM, Parcels.wrap(item));
        startingActivity.startActivity(intent);
    }

    @Override
    public void onStateChange(int state) {


        if (RevealBackgroundView.STATE_FINISHED == state) {

            mAnimationSet.play(fadeIn);

            mAnimationSet.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    startIntroAnimation();
                }
            });
            mAnimationSet.start();
        }
    }

    private void startIntroAnimation() {
        vContainer.animate().translationY(0).setDuration(400).setInterpolator(DECELERATE_INTERPOLATOR).start();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return false;
    }
}
