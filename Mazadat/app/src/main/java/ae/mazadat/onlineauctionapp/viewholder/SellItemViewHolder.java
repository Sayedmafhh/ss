package ae.mazadat.onlineauctionapp.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ae.mazadat.onlineauctionapp.R;
import butterknife.Bind;

/**
 * Created by agile on 4/1/16.
 */
public class SellItemViewHolder extends ItemViewHolder {

    @Bind(R.id.tvPltL) public TextView tvPltL;
    @Bind(R.id.tvPltR) public TextView tvPltR;
//    @Bind(R.id.ivSoldOut) public ImageView ivSoldOut;
    @Bind(R.id.tvSoldOut) public TextView tvSoldOut;
    @Bind(R.id.vPrice) public View vPrice;

    @Bind(R.id.btnBuy) public View btnBuy;

    @Bind(R.id.tvMotocycle)
    public TextView tvMotocycle;

    public SellItemViewHolder(View view) {
        super(view);
    }
    @Bind(R.id.ivPlate) public ImageView ivPlate;
}
