package ae.mazadat.onlineauctionapp.model.rest;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by agile on 4/4/16.
 */
@Parcel
public class LoginResponse {
    @SerializedName("user")
    MZUser user;

    @Nullable
    @SerializedName("msg")
    String msg;

    public MZUser getUser() {
        return user;
    }

    @Nullable
    public String getMsg() {
        return msg;
    }
}
