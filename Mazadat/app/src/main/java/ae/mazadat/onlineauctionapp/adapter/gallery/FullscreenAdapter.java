package ae.mazadat.onlineauctionapp.adapter.gallery;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.rest.MZImg;
import ae.mazadat.onlineauctionapp.util.AGUtil;
import ae.mazadat.onlineauctionapp.util.view.TouchImageView;

/**
 * Created by agile on 5/16/16.
 */
public class FullscreenAdapter extends PagerAdapter {

    private final List<MZImg> mImages;
    private FullScreenImageLoader mFullScreenImageLoader;
    // endregion

    public interface FullScreenImageLoader {
        void loadFullScreenImage(TouchImageView iv, String imageUrl, int width);
    }

    public FullscreenAdapter(List<MZImg> images) {
        mImages = images;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) container.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.gallery_fullscreen_image, null);

        TouchImageView imageView = (TouchImageView) view.findViewById(R.id.iv);

        String image = mImages.get(position).getSrc();

        Context context = imageView.getContext();
        int width = AGUtil.getScreenWidth(context);

        mFullScreenImageLoader.loadFullScreenImage(imageView, image, width);

        container.addView(view, 0);

        return view;
    }

    @Override
    public int getCount() {
        return mImages.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public void setFullScreenImageLoader(FullScreenImageLoader loader) {
        this.mFullScreenImageLoader = loader;
    }
}
