package ae.mazadat.onlineauctionapp.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.LoginEvent;
import com.crashlytics.android.answers.SignUpEvent;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZJsonObj;
import ae.mazadat.onlineauctionapp.model.rest.LoginResponse;
import ae.mazadat.onlineauctionapp.model.rest.ResetResponse;
import ae.mazadat.onlineauctionapp.model.rest.SignupResponse;
import ae.mazadat.onlineauctionapp.util.AGUtil;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import ae.mazadat.onlineauctionapp.util.rest.AGApi;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends Activity {

    // UI references.
    @Bind(R.id.username)
    EditText mUsernameView;

    @Bind(R.id.password)
    EditText mPasswordView;

    @Bind(R.id.email_sign_in_button)
    Button mButton;

    @Bind(R.id.login_progress)
    View mProgressView;

    @Bind(R.id.login_form)
    View mLoginFormView;

    @Bind(R.id.container)
    View mContainer;

    @Bind(R.id.btn_login_signup)
    Button btnSwitchSignup;


    @Bind(R.id.btn_switch_login)
    Button btnSwitchLogin;

    @Bind(R.id.tvForget)
    TextView tvForget;

    @Bind(R.id.vLogin)
    View vLogin;

    @Bind(R.id.vSignup)
    View vSignup;

    // Reset
    @Bind(R.id.vReset)
    View vReset;

    @Bind(R.id.tvCancel)
    TextView tvCancel;

    @Bind(R.id.etResetEmail)
    EditText etResetEmail;

    @Bind(R.id.btn_reset)
    Button btnReset;

    // Sign up
    @Bind(R.id.etFirstName)
    EditText etFirstName;

    @Bind(R.id.etLastName)
    EditText etLastName;

    @Bind(R.id.etEmail)
    EditText etEmail;

    @Bind(R.id.etConfirmEmail)
    EditText etConfirmEmail;

    @Bind(R.id.etPassword)
    EditText etPassword;

    @Bind(R.id.etPasswordConfirm)
    EditText etPasswordConfirm;

    @Bind(R.id.etMobile)
    EditText etMobile;

    @Bind(R.id.btn_signup)
    Button btnSignup;

    @Bind(R.id.spnCountry)
    Spinner spnCountry;

    @Bind(R.id.spnProvider)
    Spinner spnProvider;

    @Bind(R.id.spnLang)
    Spinner spnLang;

    @Bind(R.id.spnKnow)
    Spinner spnKnow;

    @Bind(R.id.chTerms)
    CheckBox chTerms;

    @Bind(R.id.tvTerms)
    TextView tvTerms;

    SessionManager session = App.getSessionManager();

    // Signup params
    Map<String, String> signupParams = new HashMap<>();


    AGApi api;
    Call<LoginResponse> loginCall;
    Call<ResetResponse> resetCall;
    Call<SignupResponse> signupCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        api = App.getRestAdapter().create(AGApi.class);

        btnSwitchSignup.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                switchToSignUp();
            }
        });

        tvCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                switchToLogin(vReset);
            }
        });

        tvForget.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                switchToReset();
            }

        });

        btnSwitchLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                switchToLogin(vSignup);
            }
        });

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        // Reset password
        btnReset.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptReset();
            }
        });

        // Sign up
        btnSignup.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptSignup();
            }
        });

        signupParams.put("agree_tos", "1");

        initSpinner(spnCountry,  "countries.json", "country");
        initSpinner(spnProvider, "providers.json", "mobile_provider");
        initSpinner(spnLang, "languages.json", "prefered_language");
        initSpinner(spnKnow, "know.json", "how_u_know");

        tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(LoginActivity.this); // Context, this, etc.
                LayoutInflater li = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

                View vi = li.inflate(R.layout.terms, null);
                WebView webview = (WebView) vi.findViewById(R.id.webview);

                String page = "terms_conditions";
                if (session.getMZLang().contentEquals("ae")) {
                    page += "_ae";
                }else {
                    page += "_en";
                }
                webview.loadUrl("file:///android_asset/data/" + page + ".html");
//                webview.loadDataWithBaseURL(null, html, "text/html", "utf-8", null);

                dialog.setContentView(vi);
                dialog.setTitle(getString(R.string.terms));
                dialog.show();
            }
        });

    }


    private void initSpinner(final Spinner spn, String file, final String param) {
        String data = AGUtil.loadJsonFile(file);

        List<MZJsonObj> items = new Gson().fromJson(data, new TypeToken<List<MZJsonObj>>() {}.getType());

        String[] spinnerArray = new String[items.size()];
        final HashMap<String, String> spinnerMap = new HashMap<String, String>();

        for (int x = 0; x<items.size(); x++)
        {
            MZJsonObj item = items.get(x);
            if (param.contentEquals("how_u_know") || param.contentEquals("prefered_language")) {
                if (session.getLang().contentEquals("ar")) {
                    spinnerMap.put(item.getName_ae(), item.getId());
                    spinnerArray[x] = item.getName_ae();
                } else {
                    spinnerMap.put(item.getName_en(), item.getId());
                    spinnerArray[x] = item.getName_en();
                }
            } else {
                spinnerMap.put(item.getName(), item.getId());
                spinnerArray[x] = item.getName();
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn.setAdapter(adapter);
        spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String itemName  = spn.getSelectedItem().toString();
                String itemCode = spinnerMap.get(itemName);

                signupParams.put(param, itemCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }


    private void switchToSignUp() {

        vLogin.animate()
                .translationY(0)
                .setDuration(500)
                .alpha(0.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        showSignUpForm();
                    }
                });
    }

    private void showSignUpForm() {
        vLogin.setVisibility(View.GONE);
        vSignup.setVisibility(View.VISIBLE);
        vSignup.setAlpha(0.0f);
        vSignup.animate()
                .setDuration(500)
                .alpha(1f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        vLogin.setVisibility(View.GONE);
                    }
                });

    }

    private void showResetForm() {
        vLogin.setVisibility(View.GONE);
        vReset.setVisibility(View.VISIBLE);
        vReset.setAlpha(0.0f);
        vReset.animate()
                .setDuration(500)
                .alpha(1f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        vLogin.setVisibility(View.GONE);
                    }
                });

    }

    private void showLoginForm() {
        vSignup.setVisibility(View.GONE);
        vLogin.setVisibility(View.VISIBLE);
        vLogin.setAlpha(0.0f);
        vLogin.animate()
                .setDuration(500)
                .alpha(1f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        vSignup.setVisibility(View.GONE);
                        vReset.setVisibility(View.GONE);
                    }
                });

    }


    private void switchToReset() {
        vLogin.animate()
                .translationY(0)
                .setDuration(500)
                .alpha(0.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        showResetForm();
                    }
                });
    }

    private void switchToLogin(View fromView) {
//        vLogin.setAlpha(0.0f);
        fromView.animate()
                .translationY(0)
                .setDuration(500)
                .alpha(0.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        showLoginForm();
                    }
                });
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid username address.
        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        }

        else if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        else if (!isUsernameValid(username)) {
            mUsernameView.setError(getString(R.string.error_invalid_username));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            AGUtil.closeKeyboard(this);
            showProgress(true);

            loginCall = api.login(username, password, session.getRegistrationId(), "android",
                    session.getMZLang().contentEquals("ae") ? "ae" : "en");
            loginCall.enqueue(new AGRetrofitCallback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    super.onResponse(call, response);

                    showProgress(false);

                    if (response.isSuccessful()) {

                        LoginResponse loginResponse = response.body();

                        if (loginResponse.getUser() != null && loginResponse.getUser().getToken() != null) {

                            // Log successful login
                            Answers.getInstance().logLogin(new LoginEvent().putSuccess(true));

                            session.createLoginSession(loginResponse);

                            // Staring MainActivity
                            Intent i = new Intent(LoginActivity.this, InitActivity.class);
                            i.addFlags(
                                    Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                            Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                            Intent.FLAG_ACTIVITY_NEW_TASK
                            );
                            startActivity(i);
                            // TODO: Set flip animation
                            //                    overridePendingTransition(R.anim.card_flip_right_in, R.anim.card_flip_left_out);
                            finish();
                        } else {
                            // Log failed login
                            Answers.getInstance().logLogin(new LoginEvent().putSuccess(false));
                            String errorMessage = getResources().getString(R.string.login_unable_to_login);
                            if (loginResponse.getMsg() != null) {
                                errorMessage = loginResponse.getMsg();
                            }
                            AGUtil.errorAlert(LoginActivity.this, "Opps!", errorMessage);
                        }
                    } else {
                        String errorMessage = getResources().getString(R.string.login_unable_to_login);
                        AGUtil.errorAlert(LoginActivity.this, "Opps!", errorMessage);
                    }

                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    super.onFailure(call, t);
                    String errorMessage = getResources().getString(R.string.login_unable_to_login);
                    AGUtil.errorAlert(LoginActivity.this, "Opps!", errorMessage);
                }
            });

//            api.login(user, new AGRestCallback<LoginResponse>() {
//                @Override
//                public void success(LoginResponse loginResponse, Response response) {
//                    showProgress(false);
//
//                    if (loginResponse.getUser() != null && loginResponse.getUser().getToken() != null) {
//
//                        SessionManager session = App.getSessionManager();
//
//                        session.createLoginSession(loginResponse);
//
//                        // Staring MainActivity
//                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
//                        i.addFlags(
//                                Intent.FLAG_ACTIVITY_CLEAR_TOP |
//                                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
//                                        Intent.FLAG_ACTIVITY_NEW_TASK
//                        );
//                        startActivity(i);
//                        // TODO: Set flip animation
//                        //                    overridePendingTransition(R.anim.card_flip_right_in, R.anim.card_flip_left_out);
//                        finish();
//                    } else {
//                        String errorMessage = getResources().getString(R.string.login_unable_to_login);
//                        AGUtil.errorAlert(LoginActivity.this, "Opps!", errorMessage);
//                    }
//                }
//
//                @Override
//                public void failure(AGRestError error) {
//                    showProgress(false);
//
//                    String errorMessage = "";
//                    if (error.getMessage() != null) {
//                        errorMessage = error.getMessage();
//                    } else {
////                    if (error == null) {
//                        errorMessage = getResources().getString(R.string.login_failed);
////                    } else {
////                        errorMessage = error.getMessage();
////                    }
//                    }
//
//                    AGUtil.errorAlert(LoginActivity.this, "Opps!", errorMessage);
//                }
//            });
        }
    }

    private boolean isUsernameValid(String username) {
        return username.length() > 2;

    }

    private boolean isEmailOrMobileValid(String value) {
        return value.length() > 2;

    }

    private boolean isMobileValid(String value) {
        return value.length() == 7;

    }

    private boolean isPasswordValid(String password) {
        return password.length() > 3;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }


    /**
     * Reset Password
     */
    private void attemptReset() {

        // Reset errors.
        etResetEmail.setError(null);

        // Store values at the time of the login attempt.
        String email = etResetEmail.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email / mobile
        if (TextUtils.isEmpty(email)) {
            etResetEmail.setError(getString(R.string.error_field_required));
            focusView = etResetEmail;
            cancel = true;
        }

        else if (!isEmailOrMobileValid(email)) {
            etResetEmail.setError(getString(R.string.error_invalid_email_mobile));
            focusView = etResetEmail;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            AGUtil.closeKeyboard(this);
            showProgress(true);

            resetCall = api.resetPwd(email, session.getMZLang().contentEquals("ae") ? "ae" : "en");
            resetCall.enqueue(new AGRetrofitCallback<ResetResponse>() {
                @Override
                public void onResponse(Call<ResetResponse> call, Response<ResetResponse> response) {
                    super.onResponse(call, response);

                    showProgress(false);

                    if (response.isSuccessful()) {

                        Log.i("ForgotPassword", response.body().getMsg("") + " " + response.body().getCode());
                        if (response.body().getMsg(getString(R.string.reset_pwd_success)).equalsIgnoreCase("success") || response.body().getMsg("").equalsIgnoreCase("")) {
                            etResetEmail.setText("");
                            switchToLogin(vReset);
                            AGUtil.successAlert(LoginActivity.this,
                                    getString(R.string.reset_pwd),
                                    response.body().getMsg(getString(R.string.reset_pwd_success)));
                        }else {
                            AGUtil.errorAlert(LoginActivity.this, "Opps!", response.body().getMsg(getString(R.string.login_unable_to_reset)));
                        }

                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();

                        String errorMessage = getString(R.string.login_unable_to_reset);
                        AGUtil.errorAlert(LoginActivity.this, "Opps!", errorMessage);
                    }

                }

                @Override
                public void onFailure(Call<ResetResponse> call, Throwable t) {
                    super.onFailure(call, t);
                    String errorMessage = getResources().getString(R.string.login_unable_to_reset);
                    AGUtil.errorAlert(LoginActivity.this, "Opps!", errorMessage);
                }
            });
        }
    }


    /**
     * Sign Up
     */
    private void attemptSignup() {

        // Check agreed to terms
        if (!chTerms.isChecked()) {
            new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getString(R.string.tos_title))
                    .setContentText(getString(R.string.tos_not_agreed))
                    .setCancelText(getString(R.string.cancel))
                    .show();
            return;
        }

        // Reset errors.
        etResetEmail.setError(null);

        // Store values at the time of the login attempt.
        String firstName    = etFirstName.getText().toString();
        String lastName     = etLastName.getText().toString();
        String email        = etEmail.getText().toString();
        String emailConfirm = etConfirmEmail.getText().toString();

        String password        = etPassword.getText().toString();
        String passwordConfirm = etPasswordConfirm.getText().toString();

        String mobile          = etMobile.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email / mobile
        if (TextUtils.isEmpty(firstName)) {
            etFirstName.setError(getString(R.string.error_field_required));
            focusView = etFirstName;
            cancel = true;
        }

        else if (TextUtils.isEmpty(lastName)) {
            etLastName.setError(getString(R.string.error_field_required));
            focusView = etLastName;
            cancel = true;
        }

        else if (TextUtils.isEmpty(email)) {
            etEmail.setError(getString(R.string.error_field_required));
            focusView = etEmail;
            cancel = true;
        }

        else if (TextUtils.isEmpty(emailConfirm)) {
            etConfirmEmail.setError(getString(R.string.error_field_required));
            focusView = etConfirmEmail;
            cancel = true;
        }

        else if (!isEmailOrMobileValid(email)) {
            etEmail.setError(getString(R.string.error_invalid_email_mobile));
            focusView = etEmail;
            cancel = true;
        }

        else if (!isEmailOrMobileValid(emailConfirm)) {
            etConfirmEmail.setError(getString(R.string.error_invalid_email_mobile));
            focusView = etConfirmEmail;
            cancel = true;
        }

        else if (!email.contentEquals(emailConfirm)) {
            etConfirmEmail.setError(getString(R.string.error_not_match_email));
            focusView = etConfirmEmail;
            cancel = true;
        }


        else if (TextUtils.isEmpty(password)) {
            etPassword.setError(getString(R.string.error_field_required));
            focusView = etPassword;
            cancel = true;
        }

        else if (TextUtils.isEmpty(passwordConfirm)) {
            etPasswordConfirm.setError(getString(R.string.error_field_required));
            focusView = etPasswordConfirm;
            cancel = true;
        }

        else if (!password.contentEquals(passwordConfirm)) {
            etPasswordConfirm.setError(getString(R.string.error_not_match_pwd));
            focusView = etPasswordConfirm;
            cancel = true;
        }

        else if (!isMobileValid(mobile)) {
            etMobile.setError(getString(R.string.error_invalid_mobile));
            focusView = etMobile;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            AGUtil.closeKeyboard(this);
            showProgress(true);

//            Map<String, String> params = new HashMap<>();
            signupParams.put("first_name", firstName);
            signupParams.put("last_name", lastName);
            signupParams.put("email", email);
            signupParams.put("email_confirm", emailConfirm);

            signupParams.put("new_password", password);
            signupParams.put("new_password2", passwordConfirm);

            signupParams.put("mobile_no", mobile);
            signupParams.put("device_id", session.getRegistrationId());

            signupCall = api.signup(signupParams);
            signupCall.enqueue(new AGRetrofitCallback<SignupResponse>() {
                @Override
                public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                    super.onResponse(call, response);

                    showProgress(false);

                    if (response.isSuccessful()) {
                        SignupResponse res = response.body();

                        if (res.getStatus().contentEquals("200")) {
                            // TODO: Reset fields when success
//                        etResetEmail.setText("");
                            switchToLogin(vSignup);

                            // Log success signup
                            Answers.getInstance().logSignUp(new SignUpEvent().putSuccess(true));

                            String msg = getString(R.string.signup_success);
                            if (res.getMsg() != null && !res.getMsg().isEmpty()) {
                                msg = res.getMsg();
                            }

                            AGUtil.successAlert(LoginActivity.this, getString(R.string.sign_up), msg);
                        } else {
                            // Log failed! signup
                            Answers.getInstance().logSignUp(new SignUpEvent().putSuccess(false)
                                    .putCustomAttribute("Error Message", res.getMsg()));
                            AGUtil.errorAlert(LoginActivity.this, "Opps!", res.getMsg());
                        }


                    } else {
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        String errorMessage = getResources().getString(R.string.login_unable_to_signup);
                        AGUtil.errorAlert(LoginActivity.this, "Opps!", errorMessage);
                    }

                }

                @Override
                public void onFailure(Call<SignupResponse> call, Throwable t) {
                    super.onFailure(call, t);
                    String errorMessage = getResources().getString(R.string.login_unable_to_signup);
                    AGUtil.errorAlert(LoginActivity.this, "Opps!", errorMessage);
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.no_change, R.anim.slide_down);
    }
}

