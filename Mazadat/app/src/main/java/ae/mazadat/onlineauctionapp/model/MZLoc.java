package ae.mazadat.onlineauctionapp.model;

import org.parceler.Parcel;
/**
 * Created by IT_3 on 9/17/2017.
 */

@Parcel
public class MZLoc {

    String latitude;
    String longitude;
    String title;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
