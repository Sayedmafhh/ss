package ae.mazadat.onlineauctionapp.ui.menu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.rest.MZResponse;
import ae.mazadat.onlineauctionapp.model.rest.MZUser;
import ae.mazadat.onlineauctionapp.util.AGUtil;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import ae.mazadat.onlineauctionapp.util.rest.AGApi;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * interface.
 */
public class ProfileFragment extends Fragment {

    AGApi api;
    Call<MZResponse> profileCall;

    SessionManager session = App.getSessionManager();
    Map<String, String> requestParams = new HashMap<>();

    @Bind(R.id.container)
    LinearLayout container;

    LayoutInflater li;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ProfileFragment() {}

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ProfileFragment newInstance(int columnCount) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);

        api = App.getRestAdapter().create(AGApi.class);
        profileCall = api.getMyProfile(requestParams);

        final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(R.color.colorPrimaryDark);
        pDialog.setTitleText(getString(R.string.loading_profile));
        pDialog.setCancelable(false);
        pDialog.show();

        li = LayoutInflater.from(getActivity());


        profileCall.enqueue(new AGRetrofitCallback<MZResponse>() {
            @Override
            public void onResponse(Call<MZResponse> call, Response<MZResponse> response) {
                super.onResponse(call, response);

                if (isAdded() && getActivity() != null) {
                    pDialog.dismiss();

                    if (response.isSuccessful()) {
                        MZResponse res = response.body();

                        MZUser user = res.getProfile();

                        if (user != null) {
                            if (user.getName() != null) {
                                _drawItem(getString(R.string.user_id), user.getId()+"", false);
                            }
                            if (user.getName() != null) {
                                _drawItem(getString(R.string.lbl_name), user.getName(), false);
                            }

                            if (user.getEmail() != null) {
                                _drawItem(getString(R.string.lbl_email), user.getEmail(), false);
                            }

                            if (user.getPhone() != null) {
                                _drawItem(getString(R.string.lbl_phone), user.getPhone(), false);
                            }

                            if (user.getLastLogin() != null) {
                                _drawItem(getString(R.string.lbl_last_login), user.getLastLogin(), false);
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MZResponse> call, Throwable t) {
                super.onFailure(call, t);
                if (isAdded() && getActivity() != null) {
                    pDialog.dismiss();
                    AGUtil.errorAlert(getActivity(), "Opps!", getString(R.string.err_unable_to_reset));
                }
            }
        });

        return view;
    }

    private void _drawItem(String lbl, String val, boolean hideHr) {
        View v = li.inflate(R.layout.lbl_val, null);
        TextView tvLbl = (TextView) v.findViewById(R.id.tvLbl);
        TextView tvVal = (TextView) v.findViewById(R.id.tvVal);

        tvLbl.setText(lbl);
        tvVal.setText(val);

        if (hideHr) {
            View hr = v.findViewById(R.id.hr);
            hr.setVisibility(View.GONE);
        }

        container.addView(v);
    }

}
