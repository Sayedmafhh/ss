package ae.mazadat.onlineauctionapp.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.rest.DashboardItem;
import ae.mazadat.onlineauctionapp.util.AGUtil;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A fragment representing a list of Items.
 * <p/>
 * interface.
 */
public class ServicesFragment extends Fragment {


    // TODO: Set
    String lang = "en";

    // Items
    @Bind(R.id.btn1)
    View btn1;

    @Bind(R.id.btn2)
    View btn2;

    @Bind(R.id.btn3)
    View btn3;

    @Bind(R.id.btn4)
    View btn4;

    @Bind(R.id.btn5)
    View btn5;

    @Bind(R.id.btn6)
    View btn6;

    SessionManager session = App.getSessionManager();

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ServicesFragment() {}

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ServicesFragment newInstance(int columnCount) {
        ServicesFragment fragment = new ServicesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_services, container, false);
        ButterKnife.bind(this, view);

        initAnimation();

        setItems();
        return view;
    }

    private void setItems() {

        List<DashboardItem> items = session.getDashboard();

        if (items != null && items.size() > 0) {
            for (int i=0; i<items.size(); i++) {
                if (items.get(i).getId() == AGConf.CAT_SERVICES &&
                    items.get(i).getSubItems() != null && items.get(i).getSubItems().size() > 0) {
                    for (int x=0; x<items.get(i).getSubItems().size(); x++) {
                        drawItem(items.get(i).getSubItems().get(x));
                    }
                } else {
                    continue;
                }
            }

            animateItems();

//            DashboardItem drawNumberItem = new DashboardItem();
//            drawNumberItem.setId(AGConf.CAT_DRAW_NUMBER);
//            drawNumberItem.setTotal(0);
//            drawNumberItem.setName(getString(R.string.db_draw_number));
//            drawItem(drawNumberItem);
//
//            DashboardItem requestNumberItem = new DashboardItem();
//            requestNumberItem.setId(AGConf.CAT_REQUEST_NUMBER);
//            requestNumberItem.setTotal(0);
//            requestNumberItem.setName(getString(R.string.db_request_number));
//            drawItem(requestNumberItem);
        } else {
            // TODO: Error!!
//                        // TODO: Show proper message
            AGUtil.errorAlert(getActivity(), "No Services!", "No Services Items!");
        }

    }

    private void drawItem(DashboardItem item) {

        switch (item.getId()) {
            case AGConf.CAT_DRAW_PLATE:
                drawDrawPlateItem(item);
                break;

            case AGConf.CAT_REQUEST_PLATE:
                drawRequestPlateItem(item);
                break;

            case AGConf.CAT_EVALUATE_PLATE:
                drawEvaluatePlaceItem(item);
                break;

            case AGConf.CAT_MIX_ADS:
                drawMixAdsItem(item);
                break;

            case AGConf.CAT_REQUEST_NUMBER:
                drawRequestNumberItem(item);
                break;

            case AGConf.CAT_DRAW_NUMBER:
                drawDrawNumberItem(item);
                break;

            default:
                return;
        }
    }


    private void openAuction(DashboardItem item, View v) {
        int[] startingLocation = new int[2];
        v.getLocationOnScreen(startingLocation);
        startingLocation[0] += v.getWidth() / 2;
        startingLocation[1] += (v.getHeight() / 2)- 60;
        int color = 0xFFFFFFFF; // 0xFFD32F2F
        ItemsActivity.startFromLocation(startingLocation, getActivity(), item, color);
        getActivity().overridePendingTransition(0, 0);
    }


    private void drawDrawPlateItem(final DashboardItem item) {
        ImageView vImg = (ImageView) btn1.findViewById(R.id.img);
        vImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_plates));
        _setItemInfo(item, btn1);

        TextView vName  = (TextView) btn1.findViewById(R.id.name);
        vName.setText(item.getAlias());
//        vName.setText(getString(R.string.db_cars));

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = v.findViewById(R.id.img);
                openAuction(item, view);
            }
        });
    }

    private void drawEvaluatePlaceItem(final DashboardItem item) {
        ImageView vImg = (ImageView) btn2.findViewById(R.id.img);
        vImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_plates));
        _setItemInfo(item, btn2);

        TextView vName  = (TextView) btn2.findViewById(R.id.name);
        vName.setText(item.getAlias());
//        vName.setText(getString(R.string.db_cars));

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = v.findViewById(R.id.img);
                openAuction(item, view);
            }
        });
    }

    private void drawRequestPlateItem(final DashboardItem item) {
        ImageView vImg = (ImageView) btn3.findViewById(R.id.img);
        vImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_plates));
        _setItemInfo(item, btn3);

        TextView vName  = (TextView) btn3.findViewById(R.id.name);
        vName.setText(item.getAlias());
//        vName.setText(getString(R.string.db_cars));

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = v.findViewById(R.id.img);
                openAuction(item, view);
            }
        });
    }

    private void drawRequestNumberItem(final DashboardItem item) {
        ImageView vImg = (ImageView) btn6.findViewById(R.id.img);
        vImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_plates)); // TODO: Set icon
        _setItemInfo(item, btn6);

        TextView vName  = (TextView) btn6.findViewById(R.id.name);
        vName.setText(item.getAlias());

        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = v.findViewById(R.id.img);
                openAuction(item, view);
            }
        });
    }

    private void drawDrawNumberItem(final DashboardItem item) {
        ImageView vImg = (ImageView) btn5.findViewById(R.id.img);
        vImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_plates));  // TODO: Set icon
        _setItemInfo(item, btn5);

        TextView vName  = (TextView) btn5.findViewById(R.id.name);
        vName.setText(item.getAlias());

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = v.findViewById(R.id.img);
                openAuction(item, view);
            }
        });
    }

    private void drawMixAdsItem(final DashboardItem item) {
        ImageView vImg = (ImageView) btn4.findViewById(R.id.img);
        vImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_services));
        _setItemInfo(item, btn4);

        TextView vName  = (TextView) btn4.findViewById(R.id.name);
        vName.setText(item.getAlias());
//        vName.setText(getString(R.string.db_cars));

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = v.findViewById(R.id.img);
                openAuction(item, view);
            }
        });
    }


    private void _setItemInfo(DashboardItem item, View btn) {

//        TextView vName  = (TextView) btn.findViewById(R.id.name);
        TextView vBadge = (TextView) btn.findViewById(R.id.badge);

//        vName.setText(getString(R.string.db_plates));

        if (item.getTotal() > 0) {
            vBadge.setText(item.getTotal() + "");
        } else {
            vBadge.setVisibility(View.GONE);
        }
    }

    private void initAnimation() {
        btn1.setScaleX(0); btn1.setScaleY(0);
        btn2.setScaleX(0); btn2.setScaleY(0);
        btn3.setScaleX(0); btn3.setScaleY(0);
        btn4.setScaleX(0); btn4.setScaleY(0);
        btn5.setScaleX(0); btn5.setScaleY(0);
        btn6.setScaleX(0); btn6.setScaleY(0);
    }

    private void animateItems() {
        int initDelay = 1400;
        int speed = 550;
        btn5.animate().scaleX(1).scaleY(1).setStartDelay(initDelay).setDuration(speed).start();
        btn4.animate().scaleX(1).scaleY(1).setStartDelay(initDelay+100).setDuration(speed).start();
        btn1.animate().scaleX(1).scaleY(1).setStartDelay(initDelay+200).setDuration(speed).start();
        btn6.animate().scaleX(1).scaleY(1).setStartDelay(initDelay+250).setDuration(speed).start();
        btn2.animate().scaleX(1).scaleY(1).setStartDelay(initDelay+350).setDuration(speed).start();
        btn3.animate().scaleX(1).scaleY(1).setStartDelay(initDelay+450).setDuration(speed).start();
    }
}
