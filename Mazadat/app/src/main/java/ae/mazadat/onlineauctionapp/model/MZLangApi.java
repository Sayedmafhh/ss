package ae.mazadat.onlineauctionapp.model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by IT_3 on 9/24/2017.
 */
@Parcel
// Todo Obaid
public class MZLangApi {
    @Nullable
    @SerializedName("commision")
    String commision;
    @Nullable
    @SerializedName("administration_fees")
    String administrationFees;

    @Nullable
    @SerializedName("quantity")
    String quantity;
    @Nullable
    @SerializedName("price")
    String price;
    @Nullable
    @SerializedName("self_pickup")
    String selfPickup;
    @Nullable
    @SerializedName("self_pickup_title_msg")
    String selfPickupTitleMsg;
    @Nullable
    @SerializedName("main_office_title")
    String mainOfficeTitle;
    @Nullable
    @SerializedName("main_office_text")
    String mainOfficeText;
    @Nullable
    @SerializedName("aswak_al_mizhar")
    String aswakAlMizhar;
    @Nullable
    @SerializedName("aswak_al_mizhar_text")
    String aswakAlMizharText;
    @Nullable
    @SerializedName("recipient_name")
    String recipientName;
    @Nullable
    @SerializedName("recipient_contact_no")
    String recipientContactNo;
    @Nullable
    @SerializedName("delivery_myplace")
    String deliveryMyplace;
    @Nullable
    @SerializedName("delivery_myplace_title_msg")
    String deliveryMyplaceTitleMsg;
    @Nullable
    @SerializedName("reciever_name")
    String recieverName;
    @Nullable
    @SerializedName("reciever_mobile")
    String recieverMobile;
    @Nullable
    @SerializedName("reciever_address")
    String recieverAddress;
    @Nullable
    @SerializedName("cart_subtotal")
    String cartSubtotal;
    @Nullable
    @SerializedName("shipping_charge")
    String shippingCharge;
    @Nullable
    @SerializedName("checkout")
    String checkout;

    @Nullable
    @SerializedName("currency")
    String currency;

    public String getCommision() {
        return commision;
    }

    public void setCommision(String commision) {
        this.commision = commision;
    }

    public String getAdministrationFees() {
        return administrationFees;
    }

    public void setAdministrationFees(String administrationFees) {
        this.administrationFees = administrationFees;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSelfPickup() {
        return selfPickup;
    }

    public void setSelfPickup(String selfPickup) {
        this.selfPickup = selfPickup;
    }

    public String getSelfPickupTitleMsg() {
        return selfPickupTitleMsg;
    }

    public void setSelfPickupTitleMsg(String selfPickupTitleMsg) {
        this.selfPickupTitleMsg = selfPickupTitleMsg;
    }

    public String getMainOfficeTitle() {
        return mainOfficeTitle;
    }

    public void setMainOfficeTitle(String mainOfficeTitle) {
        this.mainOfficeTitle = mainOfficeTitle;
    }

    public String getMainOfficeText() {
        return mainOfficeText;
    }

    public void setMainOfficeText(String mainOfficeText) {
        this.mainOfficeText = mainOfficeText;
    }

    public String getAswakAlMizhar() {
        return aswakAlMizhar;
    }

    public void setAswakAlMizhar(String aswakAlMizhar) {
        this.aswakAlMizhar = aswakAlMizhar;
    }

    public String getAswakAlMizharText() {
        return aswakAlMizharText;
    }

    public void setAswakAlMizharText(String aswakAlMizharText) {
        this.aswakAlMizharText = aswakAlMizharText;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientContactNo() {
        return recipientContactNo;
    }

    public void setRecipientContactNo(String recipientContactNo) {
        this.recipientContactNo = recipientContactNo;
    }

    public String getDeliveryMyplace() {
        return deliveryMyplace;
    }

    public void setDeliveryMyplace(String deliveryMyplace) {
        this.deliveryMyplace = deliveryMyplace;
    }

    public String getDeliveryMyplaceTitleMsg() {
        return deliveryMyplaceTitleMsg;
    }

    public void setDeliveryMyplaceTitleMsg(String deliveryMyplaceTitleMsg) {
        this.deliveryMyplaceTitleMsg = deliveryMyplaceTitleMsg;
    }

    public String getRecieverName() {
        return recieverName;
    }

    public void setRecieverName(String recieverName) {
        this.recieverName = recieverName;
    }

    public String getRecieverMobile() {
        return recieverMobile;
    }

    public void setRecieverMobile(String recieverMobile) {
        this.recieverMobile = recieverMobile;
    }

    public String getRecieverAddress() {
        return recieverAddress;
    }

    public void setRecieverAddress(String recieverAddress) {
        this.recieverAddress = recieverAddress;
    }

    public String getShippingCharge() {
        return shippingCharge;
    }

    public void setShippingCharge(String shippingCharge) {
        this.shippingCharge = shippingCharge;
    }

    public String getCartSubtotal() {
        return cartSubtotal;
    }

    public void setCartSubtotal(String cartSubtotal) {
        this.cartSubtotal = cartSubtotal;
    }
    public String getCheckout() {
        return checkout;
    }

    public void setCheckout(String cartSubtotal) {
        this.checkout = cartSubtotal;
    }

    public String getCurrency() {
        return currency;
    }
    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
