package ae.mazadat.onlineauctionapp.ui;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.HashMap;
import java.util.Map;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.MZLangApi;
import ae.mazadat.onlineauctionapp.model.TotalValue;
import ae.mazadat.onlineauctionapp.model.rest.MZResponse;
import ae.mazadat.onlineauctionapp.ui.payment.PaymentWebActivity;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import ae.mazadat.onlineauctionapp.util.rest.AGApi;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ShippingOptionFragment.OnMZItemInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ShippingOptionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ShippingOptionFragment extends Fragment {

    @Bind(R.id.quantity)
    TextView quantity;
    @Bind(R.id.price)
    TextView price;
    @Bind(R.id.shipcharges)
    TextView shipCharges;
    @Bind(R.id.adminfees)
    TextView adminFees;
    @Bind(R.id.adminfeesVal)
    TextView adminfeesVal;
    @Bind(R.id.totPrice)
    TextView totPrice;
    @Bind(R.id.shipchargesVal)
    TextView shipChargesVal;
    @Bind(R.id.quantityVal)
    TextView quantityVal;
    @Bind(R.id.priceVal)
    TextView priceVal;
    @Bind(R.id.totPriceVal)
    TextView totPriceVal;
    @Bind(R.id.btnCheckOut)
    Button btnCheckout;
    @Bind(R.id.tabHost)
    TabHost tabhost;
    @Bind(R.id.selfTitleMsg)
    TextView selfTitleMsg;
    @Bind(R.id.mainOfficeTitle)
    RadioButton mainOfficeTitle;
    @Bind(R.id.mainOfficeAddress)
    TextView mainOfficeAddress;
    @Bind(R.id.aswakOfficetitle)
    RadioButton aswakOfficetitle;
    @Bind(R.id.aswakOfficeAddress)
    TextView aswakOfficeAddress;
    @Bind(R.id.recipientName)
    TextView recipientName;
    @Bind(R.id.recipientContact)
    TextView recipientContact;
    @Bind(R.id.recieverName)
    TextView recieverName;
    @Bind(R.id.recieverContact)
    TextView recieverContact;
    @Bind(R.id.recieverAddress)
    TextView recieverAddress;
    @Bind(R.id.deliveryMyPlaceTitleText)
    TextView deliveryMyPlaceTitleText;
    @Bind(R.id.officeSelect)
    RadioGroup officeSelect;
    @Bind(R.id.txtName)
    EditText txtName;
    @Bind(R.id.txtContact)
    EditText txtContact;
    @Bind(R.id.txtRecieverName)
    EditText txtRecieverName;
    @Bind(R.id.txtRecieverContact)
    EditText txtRecieverContact;
    @Bind(R.id.txtRecieverAddress)
    EditText txtRecieverAddress;




    protected ItemsListFragment.OnMZItemInteractionListener mListener;
    Call<MZResponse> getDetailsItems;
    String currentAuction = "";
    SessionManager session = App.getSessionManager();
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    protected Call<MZResponse> gettingItemsCall;
    protected Map<String, String> filterParams = new HashMap<>();
    Map<String, String> paymentParams = new HashMap<>();
    protected int ITEMS_PER_PAGE = 30;
    int currentPage;
    protected AGApi api;
    static MZLangApi itemDesc;
    static TotalValue itemPrices;
    String radioCheckedValue;
    int officeLoc,DeliveryMode;

    public ShippingOptionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param item Parameter 1.
     * @param type Parameter 2.
     * @return A new instance of fragment ShippingOptionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ShippingOptionFragment newInstance(MZItem item, String type) {
        ShippingOptionFragment fragment = new ShippingOptionFragment();
        Bundle args = new Bundle();
        args.putParcelable(AGConf.KEY_ITEM_DATA, Parcels.wrap(item));
        args.putString(AGConf.KEY_AUCTION_TYPE, type);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }





    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        _loadTabs();
        View view = inflater.inflate(R.layout.fragment_shipping_option2, container, false);
        ButterKnife.bind(this, view);



        tabhost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                setTabColor(tabhost);
            }
        });

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                   /* boolean cancel = false;
                    View focusView = null;*/
                String mobile="" ,name = "",address="";

                if (session.isLoggedIn()) {
                    if (tabhost.getTabWidget().getChildAt(0).isSelected()){

                        DeliveryMode =1;
                        int checkedRadioButtonId = officeSelect.getCheckedRadioButtonId();
                        name = txtName.getText().toString();
                        mobile = txtContact.getText().toString();


                        if (checkedRadioButtonId == -1) {
                            Toast.makeText(getActivity(), "select any office from following", Toast.LENGTH_SHORT).show();
                        }
                        else{

                            if (checkedRadioButtonId == R.id.mainOfficeTitle) {
                                radioCheckedValue = itemDesc.getMainOfficeText();
                                //   Toast.makeText(getActivity(), radioCheckedValue, Toast.LENGTH_SHORT).show();
                                officeLoc = 1;


                            }
                            if (checkedRadioButtonId == R.id.aswakOfficetitle) {
                                radioCheckedValue = itemDesc.getAswakAlMizharText();
                                officeLoc = 2;
                                // Toast.makeText(getActivity(), radioCheckedValue, Toast.LENGTH_SHORT).show();
                            }
                        }
                        if (!name.isEmpty() && !mobile.isEmpty()){
                            if (checkedRadioButtonId != -1)
                            paymentActivity(itemPrices.getCartSubtotal(),radioCheckedValue,officeLoc,DeliveryMode,name,mobile,address="");
                            else {
                                Toast.makeText(getActivity(), "select any office from following", Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(getActivity(), "Fill all fields", Toast.LENGTH_SHORT).show();
                        }

                    }

                    else if (tabhost.getTabWidget().getChildAt(1).isSelected()){

                        name = txtRecieverName.getText().toString();
                        mobile = txtRecieverContact.getText().toString();
                        address = txtRecieverAddress.getText().toString();
                        DeliveryMode =2;

                        if (!name.isEmpty() && !mobile.isEmpty() && !address.isEmpty()){
                            paymentActivity(itemPrices.getCartSubtotal(),radioCheckedValue,officeLoc,DeliveryMode, name, mobile, address);
                        }else {
                            Toast.makeText(getActivity(), "Fill all fields", Toast.LENGTH_SHORT).show();
                        }

                    }




                    // paymentActivity(Integer.parseInt(subTotal));
                } else {
/*                        Intent intent = new Intent(ShippingGlobalVillageBuyNow.this, LoginActivity.class);
                        ShippingGlobalVillageBuyNow.this.startActivity(intent);
                        ShippingGlobalVillageBuyNow.this.overridePendingTransition(R.anim.slide_up,R.anim.no_change);*/

                }
            }
        });




/*
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
// Add code Here
            }
        });*/



        return view;

    }


    private void paymentActivity(int subTotal, String officeAddress, int loc, int mode, String name, String mobile, String address) {
        //Todo Obaid
        PaymentWebActivity.startDirectPayment("http://mazadat.ae/api_v1/payment?address="+address+"&loc="+loc+"&mobile="+mobile+"&name="+name+"&qty="+itemPrices.getQuantity()+"&lang=" +
                this.session.getMZLang() + "&category=" + ItemDetailsFragment.mItem.getCategory() + "&amount=" + subTotal +
                "&item_id=" + ItemDetailsFragment.mItem.getId(), getActivity());

    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ItemsListFragment.OnMZItemInteractionListener) {
            mListener = (ItemsListFragment.OnMZItemInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMZItemInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnMZItemInteractionListener {
        // TODO: Update argument type and name
        void onMZItemInteraction(MZItem item, String type);
    }

    private void _loadTabs() {
        api = App.getRestAdapter().create(AGApi.class);

        paymentParams.put("lang",this.session.getMZLang());
        paymentParams.put("item_id",String.valueOf(ItemDetailsFragment.mItem.getId()));
        paymentParams.put("cat_id",String.valueOf(ItemDetailsFragment.mItem.getCategory()));

        getDetailsItems = api.getBuyNow(paymentParams);
        final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(R.color.colorPrimaryDark);
        pDialog.setTitleText("Loading Package Details");
        pDialog.setCancelable(false);
        pDialog.show();
        getDetailsItems.enqueue(new AGRetrofitCallback<MZResponse>() {
            @Override
            public void onResponse(Call<MZResponse> call, Response<MZResponse> response) {
                if (response.isSuccessful()) {

                    MZResponse MZResponse = response.body();
                    //   mItem = response.body();
                    itemDesc = MZResponse.getLangApi();
                    itemPrices = MZResponse.getTotalValue();

                    quantity.setText(itemDesc.getQuantity());
                    price.setText(itemDesc.getPrice());
                    adminFees.setText(itemDesc.getAdministrationFees());
                    selfTitleMsg.setText(itemDesc.getSelfPickupTitleMsg());
                    mainOfficeTitle.setText(itemDesc.getMainOfficeTitle());
                    mainOfficeAddress.setText(itemDesc.getMainOfficeText());
                    aswakOfficetitle.setText(itemDesc.getAswakAlMizhar());
                    aswakOfficeAddress.setText(itemDesc.getAswakAlMizharText());
                    recipientName.setText(itemDesc.getRecipientName());
                    recipientContact.setText(itemDesc.getRecipientContactNo());
                    recieverName.setText(itemDesc.getRecieverName());
                    recieverContact.setText(itemDesc.getRecieverMobile());
                    recieverAddress.setText(itemDesc.getRecieverAddress());
                    deliveryMyPlaceTitleText.setText(itemDesc.getDeliveryMyplaceTitleMsg());
                    totPrice.setText(itemDesc.getCartSubtotal());
                    btnCheckout.setText(itemDesc.getCheckout());
                    shipCharges.setText(itemDesc.getShippingCharge());


                    priceVal.setText(itemPrices.getPrice());
                    quantityVal.setText(String.valueOf(itemPrices.getQuantity()));
                    adminfeesVal.setText(itemPrices.getAdministrationFees());
                    shipChargesVal.setText(itemPrices.getShippingCharge());
                    totPriceVal.setText(String.valueOf(itemPrices.getCartSubtotal()) + " " + itemDesc.getCurrency());

                    tabhost.setup();


                    TabHost.TabSpec spec1 = tabhost.newTabSpec("Self Pickupp");
                    spec1.setIndicator("Self Pickup");
                    spec1.setContent(R.id.tab1);

                    //TabWidget tabwidg = (TabWidget) tabhost.findViewById(android.R.id.tabs);
                    //View tabView = tabwidg.getChildTabViewAt(0);
                    tabhost.addTab(spec1); // Adding photos tab

                    TabHost.TabSpec spec2 = tabhost.newTabSpec("Deliveryy"); // Create a new TabSpec using tab host
                    spec2.setIndicator("Delivery at My place");
                    spec2.setContent(R.id.tab2);
                    tabhost.addTab(spec2); // Adding songs tab

                    TextView x = (TextView) tabhost.getTabWidget().getChildAt(0).findViewById(android.R.id.title);
                    TextView y = (TextView) tabhost.getTabWidget().getChildAt(1).findViewById(android.R.id.title);

                        tabhost.getTabWidget().getChildAt(0)
                                .setBackgroundColor(Color.parseColor("#DD6B55"));  // unselected

                    x.setText(itemDesc.getSelfPickup());
                    x.setTextColor(Color.WHITE);
                    x.setTextSize(10);

                    y.setText(itemDesc.getDeliveryMyplace());
                    y.setTextSize(10);

                    pDialog.dismiss();

                } else {
                    Toast.makeText(getActivity(), "error cant get data", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<MZResponse> call, Throwable t) {
                t.getMessage();
                if (isAdded() && getActivity() != null) {
                }
            }
        });
    }
    public static void setTabColor(TabHost tabhost) {

        for (int i = 0; i < tabhost.getTabWidget().getChildCount(); i++) {
            tabhost.getTabWidget().getChildAt(i)
                    .setBackgroundColor(Color.TRANSPARENT); // unselected
            TextView tv = (TextView) tabhost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //for Selected Tab
            tv.setTextColor(Color.BLACK);
        }

        //tabhost.getTabWidget().setCurrentTab(0);
        tabhost.getTabWidget().getChildAt(tabhost.getCurrentTab())
                .setBackgroundColor(Color.parseColor("#DD6B55")); // selected
        TextView tv = (TextView) tabhost.getCurrentTabView().findViewById(android.R.id.title); //for Selected Tab
        tv.setTextColor(Color.WHITE);
        // //have
        // to
        // change
    }

}
