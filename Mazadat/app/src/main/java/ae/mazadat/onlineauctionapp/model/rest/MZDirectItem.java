package ae.mazadat.onlineauctionapp.model.rest;

import org.parceler.Parcel;

/**
 * Created by agile on 5/30/16.
 */
@Parcel
public class MZDirectItem {

    MZDirectTotal total;

    public MZDirectTotal getTotal() {
        return total;
    }

    @Override
    public String toString() {
        return "MZDirectItem{" +
                "total=" + total +
                '}';
    }
}
