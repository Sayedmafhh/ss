package ae.mazadat.onlineauctionapp.model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

import ae.mazadat.onlineauctionapp.model.rest.MZImg;
import ae.mazadat.onlineauctionapp.model.rest.MZValue;

/**
 * Created by agile on 4/1/16.
 */
@Parcel
public class MZItemDetails {

    @SerializedName("number")
    String plateNumber;

    @SerializedName("letter")
    String plateLetter;

    @SerializedName("plate_type")
    String plateType;

    // Cars
    @Nullable
    @SerializedName("img")
    List<MZImg> images;

    @Nullable
    @SerializedName("location")
    String locationCoords;

    @Nullable
    @SerializedName("branch_locations")
    List<MZLoc> brLocation;


    @Nullable
    @SerializedName("featured")
    List<MZImg> featuredImage;

    @Nullable
    @SerializedName("details_to_show")
    List<MZValue> values;

    @Nullable
    @SerializedName("model")
    String model;

    @Nullable
    @SerializedName("make")
    String make;

    @Nullable
    @SerializedName("year")
    String year;

    @Nullable
    @SerializedName("notes")
    String notes;

    // Du
    @Nullable
    @SerializedName("package_name")
    String duPackage;

    @Nullable
    @SerializedName("package_id")
    String packageId;

    // USA cars  TODO : Obaid shah

    @Nullable
    @SerializedName("shareUrl")
    String shareUrl;

    @Nullable
    @SerializedName("vin")
    String vin;


    @Nullable
    @SerializedName("offer")
    int offer;

    @Nullable
    @SerializedName("fuel")
    String fuel;

    @Nullable
    @SerializedName("engine")
    String engine;

    @Nullable
    @SerializedName("drive")
    String drive;

    @Nullable
    @SerializedName("bodyStyle")
    String bodyStyle;

    @Nullable
    @SerializedName("exteriorColor")
    String exteriorColor;

    @Nullable
    @SerializedName("miles")
    String miles;

    @Nullable
    @SerializedName("fees")
    String fees;


    @Nullable
    @SerializedName("terms_condition")
    String termsCondition;

    @Nullable
    @SerializedName("h_t_offer")
    String hTOffer;

    @Nullable
    @SerializedName("h_t_buy")
    String hTBuy;

    @Nullable
    @SerializedName("shipping_details")
    String shippingDetails;

    @Nullable
    @SerializedName("report")
    MZReport report;



    // Watches
    String description;
    String name;

    // Properties
    String desc;

    public String getNumber() {
        return plateNumber;
    }

    public String getPlateLetter() {
        return plateLetter;
    }

    public String getPlateType() {
        return plateType == null ? "1" : plateType;
    }

    public List<MZImg> getImages() {
        return images;
    }

    @Nullable
    public List<MZValue> getValues() {
        return values;
    }

    @Nullable
    public List<MZImg> getFeaturedImage() {
        return featuredImage;
    }

    @Nullable
    public String getModel() {
        return model;
    }

    @Nullable
    public String getMake() {
        return make;
    }

    @Nullable
    public String getNotes() {
        return notes;
    }

    @Nullable
    public String getYear() {
        return year;
    }

    @Nullable
    public MZReport getReport() {
        return report;
    }

    public String getDuPackage() {
        return duPackage;
    }

    @Nullable
    public String getLocationCoords() {
        return locationCoords;
    }

    public String getPackageId() {
        return packageId;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public List<MZLoc> getBranchLocations() {
        return brLocation;
    }

    public void setBranchLocations(List<MZLoc> branchLocations) {
        this.brLocation = branchLocations;
    }
// USA CARs TODO OBAID shah



    public int getOffer() {
        return offer;
    }
    public String getShareUrl() {
        return shareUrl;
    }

    public String getVin() {
        return vin;
    }



    public String getFuel() {
        return fuel;
    }

    public String getEngine() {
        return engine;
    }

    public String getDrive() {
        return drive;
    }

    public String getBodyStyle() {
        return bodyStyle;
    }

    public String getExteriorColor() {
        return exteriorColor;
    }

    public String getMiles() {
        return miles;
    }

    public String getFees() {
        return fees;
    }
    public String getTermsCondition(){
        return termsCondition;
    }
    public String gethTOffer() {
        return hTOffer;
    }
    public String gethTBuy() {
        return hTBuy;
    }
    public String getShippingDetails() {
        return shippingDetails;
    }


    @Override
    public String toString() {
        return "MZItemDetails{" +
                "plateNumber='" + plateNumber + '\'' +
                ", plateLetter='" + plateLetter + '\'' +
                ", plateType='" + plateType + '\'' +
                ", images=" + images +
                ", locationCoords='" + locationCoords + '\'' +
                ", featuredImage=" + featuredImage +
                ", values=" + values +
                ", model='" + model + '\'' +
                ", make='" + make + '\'' +
                ", year='" + year + '\'' +
                ", notes='" + notes + '\'' +
                ", duPackage='" + duPackage + '\'' +
                ", packageId='" + packageId + '\'' +
                ", description='" + description + '\'' +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}
