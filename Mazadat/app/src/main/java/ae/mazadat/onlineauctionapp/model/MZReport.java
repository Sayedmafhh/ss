package ae.mazadat.onlineauctionapp.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by IT_3 on 9/24/2017.
 */

@Parcel
    public class MZReport {

        @SerializedName("title")
        String title;

        @SerializedName("url")
        String url;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }


    }


