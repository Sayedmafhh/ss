package ae.mazadat.onlineauctionapp.model;

/**
 * Created by agile on 4/25/16.
 */
public class MZJsonObj {

    String id;
    String name;

    String name_ae;
    String name_en;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getName_ae() {
        return name_ae;
    }

    public String getName_en() {
        return name_en;
    }
}
