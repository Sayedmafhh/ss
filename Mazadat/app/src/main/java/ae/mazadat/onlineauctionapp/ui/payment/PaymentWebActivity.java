package ae.mazadat.onlineauctionapp.ui.payment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.ui.BaseActivity;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import butterknife.Bind;

public class PaymentWebActivity extends BaseActivity {

    @Bind(R.id.webview)
    WebView mWebview;

    @Bind(R.id.progressView)
    ProgressBar progressView;

    Map<String, String> headers;

    boolean isDirect = false;
    boolean isPlateEvaluation = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasDrawer(false);
        setContentView(R.layout.activity_payment_web);

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        }


        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
            actionBar.setHomeAsUpIndicator(upArrow);
        }

        this.setTitle("");


        Intent intent = getIntent();
        String url = intent.getStringExtra(AGConf.KEY_PAYMENT_URL);

        isDirect = intent.getBooleanExtra(AGConf.KEY_DIRECT_PAYMENT, false);

        if (url == null) {
            Toast.makeText(this, getString(R.string.err_unable_to_do_payment),
                    Toast.LENGTH_LONG).show();
            finish();
//            overridePendingTransition(R.anim.scale_in, R.anim.hide_bottom_out); // TODO: SET
        }


        final WebSettings settings = mWebview.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAppCacheEnabled(false);
        settings.setBuiltInZoomControls(false);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        settings.setAppCacheEnabled(true);
        settings.setAppCachePath("/data/data/" + AGConf.PACKAGE + "/cache");

        headers = new HashMap<String, String>();
        SessionManager session = App.getSessionManager();
        headers.put("X-MZ-token", session.getToken());

        mWebview.setWebChromeClient(new AGChromeClient());
        mWebview.setWebViewClient(new AGWebViewClient());
//        WebSettings settings = webView.getSettings();
//        settings.setDomStorageEnabled(true);
        mWebview.loadUrl(url, headers);
        System.out.println("url *** "+url+" headers **"+headers);
    }


    public class AGChromeClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            if (progressView != null) {
                if (newProgress == 100) {
                    setTitle("");
                    progressView.setVisibility(View.GONE);
                } else {
                    setTitle("Please wait...");
                    progressView.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public class AGWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            System.out.println("uul "+url);
            // Finish payment process
            if (url != null && url.contains("/return_to_app_success")) {
                // Success
                if (isDirect) {
                    Toast.makeText(PaymentWebActivity.this, getString(R.string.done_direct_sucessfully), Toast.LENGTH_LONG).show();
                } else if(isPlateEvaluation) {
                    Toast.makeText(PaymentWebActivity.this, getString(R.string.done_request_plate_sucessfully), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(PaymentWebActivity.this, getString(R.string.deposited_sucessfully), Toast.LENGTH_LONG).show();
                }
                finish();
            } else if (url != null && url.contains("/return_to_app_faliure")) { // TODO: Check failure or faiure
                // Error
                Uri uri = Uri.parse(url);
                String errMsg = uri.getQueryParameter("message");
                String errCode = uri.getQueryParameter("code");
                String msg = "Error " + errCode + "; " + errMsg;
                finish();
                Toast.makeText(PaymentWebActivity.this, msg, Toast.LENGTH_SHORT).show();
            } else {
                view.loadUrl(url, headers);
                System.out.println("ErlHeader == "+url+"  "+headers);
                view.setWebChromeClient(new AGChromeClient());
                view.setWebViewClient(new AGWebViewClient());
            }
            return true;
        }

    }


    @Override
    protected void onPause() {
        super.onPause();
        try {
            Class.forName("android.webkit.WebView").getMethod("onPause", (Class[]) null).invoke(mWebview, (Object[]) null);
        } catch (IllegalAccessException e) {
        } catch (InvocationTargetException e) {
        } catch (NoSuchMethodException e) {
        } catch (ClassNotFoundException e) {
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        // Save the state of the WebView
        mWebview.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        // Restore the state of the WebView
        mWebview.restoreState(savedInstanceState);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
//        overridePendingTransition(R.anim.scale_in, R.anim.hide_bottom_out); // TODO: SET
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(event.getAction() == KeyEvent.ACTION_DOWN){
            switch(keyCode)
            {
                case KeyEvent.KEYCODE_BACK:
                    if(mWebview.canGoBack()){
                        mWebview.goBack();
                    } else {
                        onBackPressed();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Start activity
     * @param url
     * @param startingActivity Activity
     */
    public static void startPayment(String url, Activity startingActivity) {
        Intent intent = new Intent(startingActivity, PaymentWebActivity.class);
        intent.putExtra(AGConf.KEY_PAYMENT_URL, url);
        startingActivity.startActivity(intent);
        startingActivity.finish();
//        startingActivity.overridePendingTransition(R.anim.show_bottom_in, R.anim.scale_out); // TODO: SET
    }


    public static void startDirectPayment(String url, Activity startingActivity) {

        Intent intent = new Intent(startingActivity, PaymentWebActivity.class);
        intent.putExtra(AGConf.KEY_PAYMENT_URL, url);
        intent.putExtra(AGConf.KEY_DIRECT_PAYMENT, true);
        startingActivity.startActivity(intent);
        startingActivity.finish();
//        startingActivity.overridePendingTransition(R.anim.show_bottom_in, R.anim.scale_out); // TODO: SET
    }

    public static void startEvaluatePlatePayment(String url, Activity startingActivity) {
        Intent intent = new Intent(startingActivity, PaymentWebActivity.class);
        intent.putExtra(AGConf.KEY_PAYMENT_URL, url);
        intent.putExtra(AGConf.KEY_EVALUATE_PLATE_PAYMENT, true);
        startingActivity.startActivity(intent);
        startingActivity.finish();
//        startingActivity.overridePendingTransition(R.anim.show_bottom_in, R.anim.scale_out); // TODO: SET
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }

        return true;
    }
}
