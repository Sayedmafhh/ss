package ae.mazadat.onlineauctionapp.viewholder;

import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import butterknife.Bind;

/**
 * Created by agile on 4/1/16.
 */
public class GridItemViewHolder extends ItemViewHolder {

    @Bind(R.id.ivBg) public ImageView ivBg;
    @Nullable
    @Bind(R.id.tvBids) public TextView tvBids;
    @Nullable
    @Bind(R.id.tvItemName) public TextView tvModel;
    @Nullable
    @Bind(R.id.tvPrice) public TextView tvPrice;

    @Nullable
    @Bind(R.id.carName) public TextView carName;

    @Nullable
    @Bind(R.id.carTitle) public TextView carTitle;

    @Nullable
    @Bind(R.id.buyOffer) public TextView buyOffer;

    @Nullable
    @Bind(R.id.currentOffer) public TextView currentOffer;

    @Nullable
    @Bind(R.id.odometer) public TextView odometer;

    @Nullable
    @Bind(R.id.lotNo) public TextView lotNo;
//    @Bind(R.layout.car_price_counter) public ViewGroup car_price_layout;


    public GridItemViewHolder(View view) {
        super(view);
        auctionType = AGConf.KEY_AUCTION_GRID;
    }
}
