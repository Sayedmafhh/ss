package ae.mazadat.onlineauctionapp.ui;

import android.support.v4.view.GravityCompat;

public class DrawerActivity extends BaseActivity {


    @Override
    public void onBackPressed() {
        if (hasDrawer) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }
        else {
            super.onBackPressed();
        }
    }


//
//    @Override
//    public void onListFragmentInteraction(Dashboard.Item item) {
//        openAuction(item, this);
////        Toast.makeText(this, "DASH  Click", Toast.LENGTH_SHORT).show();
//    }


}
